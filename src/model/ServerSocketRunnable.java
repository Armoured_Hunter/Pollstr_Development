/*
Author: 	Jinesh Bhatt
Author:		Silviu Riley	
Author:		Joshua Muysson
Class:		Web Enterprise Applications 
Project:	Pollster Assignment 4 Version-0.3
Teacher: 	Douglas King
Date:		2018-03-28
File:		ServerSocketRunnable.java
Purpose:	Receive and send objects from and to Controller and connect to the database
*/

package model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import library.ControllerModelLibrary.*;

public class ServerSocketRunnable implements Runnable {
	// connection to the database
	private static DBManager dbManager = new DBManager();
	private Socket incoming;
	
	public ServerSocketRunnable(Socket incoming) {
		this.incoming = incoming;
	}

	@Override
	public void run() {
		// Streams to handle objects
		ObjectInputStream inStream;
		ObjectOutputStream outStream;
		// Types of objects to receive and send
		Object receivedObject;
		LoginRequest login;
		LoginResponse loginResponse = null;
		SignUp signup;
		SigninResponse signupResponse;
		
		// creates and checks for the db
		System.out.println("ServerSocket: Checking connection to database");
		
		if (dbManager.createDB()) {
			System.out.println("ServerSocket: Database is online");
		}
		else { 
			System.out.println("ServerSocket: Database couldn't be created");
		}
		
		// process objects
		try {
			// connect to streams
			inStream = new ObjectInputStream(incoming.getInputStream());
			outStream = new ObjectOutputStream(incoming.getOutputStream());
			
			// store the object from the controller
			receivedObject = inStream.readObject();

			// check the type of object and act accordingly
			if (receivedObject instanceof SignUp) {
				// for signup object is received check the db if the user is unique
				// if not then create the user
				signup = (SignUp)receivedObject;

				if (dbManager.queryTeacher(signup.getUsername())) {
					//user already exists
					signupResponse = new SigninResponse(false, true, "A user with this username already exists");
				}
				else {
					//user needs to be created
					if (dbManager.createTeacher(signup)) {
						// user was created
						signupResponse = new SigninResponse(true, true, "");
					}
					else {
						// user was NOT created
						signupResponse = new SigninResponse(true, false, "Error submitting data into the database. Please try again");
					}
				}
				
				// send the response back to controller
				outStream.writeObject(signupResponse);
			}
			else if (receivedObject instanceof LoginRequest) {
				// for loginRequestobject check for the user type and 
				// process it appropriately
				login = (LoginRequest)receivedObject;
				
				// Handle different types
				switch (login.getPer()) {
				case STUDENT: 
					// students don't have a password
					// if they don't already exist, they're created
					// if they do exist, they're logged in
					
					if (dbManager.queryStudentHasPreviouslyLogedIn(login.getUsername(), login.getStudentCode())) {
						// student has signed in before
						loginResponse = dbManager.queryQuizByStudent(dbManager.getStudentID(login), login.getStudentCode());
					}
					else {
						//username not found
						//create student
						
						loginResponse = dbManager.createStudent(login);
					}
					break;
				case TEACHER:
					// teachers have passwords
					if (dbManager.queryTeacher(login.getUsername())) {
						//user already exists
						loginResponse = dbManager.getTeacherInformation(login.getUsername());
					}
					else {
						//username doesn't exist, login fails
						loginResponse = new LoginResponse(false);
					}
					break;
				case ADMIN:
					//TODO:: admin user info return as login response
					loginResponse = dbManager.queryAdmin(login.getUsername());
				break;
				}
				
				// send the login response back to controller
				outStream.writeObject(loginResponse);
			}
			else if(receivedObject instanceof NewClassCode) {
				// send the class code added object back to controller
				outStream.writeObject(new ClassCodeAdded(dbManager.createClassCode((NewClassCode)receivedObject)));
			} 
			else if(receivedObject instanceof CreateQuiz) {
				// send the quiz created object back to controller
				outStream.writeObject(dbManager.createQuiz((CreateQuiz) receivedObject));
			}
			else if(receivedObject instanceof FetchQuizWithID) {
				// send the quiz conetents object back to controller
				outStream.writeObject(dbManager.quizContents((FetchQuizWithID)receivedObject));
			}
			else if(receivedObject instanceof AdminDataRequest) {
				// send the database set object back to controller
				outStream.writeObject(dbManager.getDatabaseData()); 
			}
			else if(receivedObject instanceof StudentAnswers) {
				// send the answers added object back to controller
				outStream.writeObject(dbManager.EnterStudentAnswer((StudentAnswers) receivedObject));
			}
			else if(receivedObject instanceof RequestDataFromTeacher) {
				// send the answers added object back to controller
				outStream.writeObject(dbManager.getTeacherData(((RequestDataFromTeacher) receivedObject).getUserName()));
			}
			else if(receivedObject instanceof ValidationRequest) {
				// Validate the teacher and send confirmation back to the user
				outStream.writeObject(dbManager.ValidateTeacher((ValidationRequest) receivedObject));
			}
			else if(receivedObject instanceof UpdateTeachersDatabase) {
				outStream.writeObject(dbManager.updateDatabaseForTeacher((UpdateTeachersDatabase) receivedObject));
			}
			else if(receivedObject instanceof AssignClassNames) {
				outStream.writeObject(dbManager.addClassCodeName((AssignClassNames) receivedObject));
			}
		}
		catch (IOException ioe) {
			System.out.println("ServerSocket: Client at " + incoming.getInetAddress() + ":" + incoming.getPort() + "has disconnected.");
			System.out.println(ioe.getMessage());
		}
		catch (ClassNotFoundException cnfe) {
			System.out.println("ServerSocket: Error processing received object");
			System.out.println(cnfe.getMessage());
		}
		finally {
			// clean up the connection
			try {
				incoming.close();
			}
			catch (IOException ioe) {
				System.out.println("ServerSocket: Error closing socket.");
				System.out.println(ioe.getMessage());
			}
		}
	}
}