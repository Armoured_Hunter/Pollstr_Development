/*
Author: 	Jinesh Bhatt
Author:		Silviu Riley
Author:		Joshua Muysson
Class:		Web Enterprise Applications 
Project:	Pollster Assignment 4 Version-0.3
Teacher: 	Douglas King
Date:		2018-03-28
File:		DBManager.java
Purpose:	Handle all database operations
 */

package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import library.DBHelper;
import library.ControllerModelLibrary;
import library.ControllerModelLibrary.*;
import library.ControllerModelLibrary.CreateQuiz.Answer;

public class DBManager {
	// Database connection info
	private final static String URL = "jdbc:mysql://" + ControllerModelLibrary.MODEL_ADDRESS + ":3306/";
	private final static String USER_NAME = "root";
	private final static String PASSWORD = "Password";
	private final static String DB_NAME = "pollstr";
	private static Connection dbConnection;

	public DBManager() {
		DBManager.startConnection();
	}

	// connect to database
	private static void startConnection() {
		try {
			dbConnection = DriverManager.getConnection(URL, USER_NAME, PASSWORD);

			System.out.println("DBManager: Database server is reachable");
		} catch (SQLException sqle) {
			System.out.println("DBManager: Database server is not reachble");
			System.out.println(sqle.getMessage());
		}
	}

	// create and populate the database if it doesn't already exist
	public boolean createDB() {
		ResultSet databases;
		boolean dbExists = false;

		System.out.println("DBManager: Looking for database " + DB_NAME);

		try {
			// collect list of existing databases on server
			databases = dbConnection.getMetaData().getCatalogs();

			// try and find pollstr
			while (databases.next()) {
				if (databases.getString(1).equals(DB_NAME)) {
					dbExists = true;

					break;
				}
			}
		} catch (SQLException sqle) {
			System.out.println("DBManager: Error looking for database " + DB_NAME);
			System.out.println(sqle.getMessage());

			return false;
		}

		// create and populate database
		if (dbExists == false) {
			System.out.println("DBManager: Database " + DB_NAME + " does not exist. Creating and populating it now.");

			try {
				// create database and tables
				dbConnection.prepareStatement(DBHelper.CREATE_DB_POLLSTR).executeUpdate();
				dbConnection.prepareStatement(DBHelper.USE_POLLSTR).executeUpdate();
				dbConnection.prepareStatement(DBHelper.CREATE_TBL_TEACHERS).executeUpdate();
				dbConnection.prepareStatement(DBHelper.CREATE_TBL_ADMINS).executeUpdate();
				dbConnection.prepareStatement(DBHelper.CREATE_TBL_CLASS_CODES).executeUpdate();
				dbConnection.prepareStatement(DBHelper.CREATE_TBL_STUDENTS).executeUpdate();
				dbConnection.prepareStatement(DBHelper.CREATE_TBL_QUIZZES).executeUpdate();
				dbConnection.prepareStatement(DBHelper.CREATE_TBL_QUESTION_TYPES).executeUpdate();
				dbConnection.prepareStatement(DBHelper.CREATE_TBL_QUESTIONS).executeUpdate();
				dbConnection.prepareStatement(DBHelper.CREATE_TBL_ANSWERS).executeUpdate();
				dbConnection.prepareStatement(DBHelper.CREATE_TBL_STUDENT_ANSWERS).executeUpdate();
				dbConnection.prepareStatement(DBHelper.INSERT_QUIZTYPE_MULTIPLE).executeUpdate();
				dbConnection.prepareStatement(DBHelper.INSERT_QUIZTYPE_TRUEFALSE).executeUpdate();
				dbConnection.prepareStatement(DBHelper.INSERT_QUIZTYPE_SHORT).executeUpdate();

				// populate teachers
				for (ControllerModelLibrary.SignUp teacher : DBHelper.TEACHER_DATA) {
					createTeacher(teacher);
					ValidateTeacher(new ValidationRequest(teacher.getUsername()));
				}

				// populate admin
				for (ControllerModelLibrary.SignUp admin : DBHelper.ADMIN_DATA) {
					createAdmin(admin);
				}

				// verify that the database was created
				databases = dbConnection.getMetaData().getCatalogs();

				while (databases.next()) {
					if (databases.getString(1).equals(DB_NAME)) {
						dbExists = true;
						break;
					}
				}

				if (dbExists == true) {
					System.out.println("DBManager: Database " + DB_NAME + " created successfully");
				}

				// dbConnection.close();

				return true;
			} catch (SQLException sqle) {
				System.out.println("DBManager: Error while creating and populating database " + DB_NAME);
				System.out.println(sqle.getMessage());

				return false;
			}
		}

		System.out.println("DBManager: Database " + DB_NAME + " exists");

		return true;
	}

	// create a user in the database
	public boolean createTeacher(SignUp teacher) {
		try {
			dbConnection.prepareStatement(DBHelper.USE_POLLSTR).executeUpdate();

			PreparedStatement pst = dbConnection.prepareStatement(DBHelper.DELETE_TEACHER_BY_USERNAME);
			pst.setString(1, teacher.getUsername());
			if (pst.executeUpdate() != 0) {
				System.out.println(String.format("Unverified user %s has been removed", teacher.getUsername()));
			}

			pst = dbConnection.prepareStatement(DBHelper.INSERT_TEACHER);
			pst.setString(1, teacher.getUsername());
			pst.setString(2, teacher.getFirstName());
			pst.setString(3, teacher.getLastName());
			pst.setString(4, teacher.getEmail());
			pst.setString(5, teacher.getPassword());
			pst.executeUpdate();

			// success
			return true;
		} catch (SQLException sqle) {
			System.out.println("DBManager: Error creating user " + teacher.getUsername());
			System.out.println(sqle.getMessage());

			return false;
		}
	}

	public ValidationComplete ValidateTeacher(ValidationRequest teacher) {

		PreparedStatement pst;

		try {
			dbConnection.prepareStatement(DBHelper.USE_POLLSTR).executeUpdate();

			pst = dbConnection.prepareStatement(DBHelper.SELECT_TEACHER_VALIDATION);
			pst.setString(1, teacher.getUsername());

			if (pst.executeQuery().first())
				return new ValidationComplete(false);

			pst = dbConnection.prepareStatement(DBHelper.UPDATE_TEACHER_VAIDATION);
			pst.setString(1, teacher.getUsername());
			pst.executeUpdate();

			return new ValidationComplete(true);

		} catch (SQLException e) {
			e.printStackTrace();
			return new ValidationComplete(false);
		}
	}

	// create a user in the database
	public boolean createAdmin(SignUp admin) {
		try {
			dbConnection.prepareStatement(DBHelper.USE_POLLSTR).executeUpdate();

			PreparedStatement pst = dbConnection.prepareStatement(DBHelper.INSERT_ADMIN);
			pst.setString(1, admin.getUsername());
			pst.setString(2, admin.getFirstName());
			pst.setString(3, admin.getLastName());
			pst.setString(4, admin.getEmail());
			pst.setString(5, admin.getPassword());
			pst.executeUpdate();
		} catch (SQLException sqle) {
			System.out.println("DBManager: Error creating user " + admin.getUsername());
			System.out.println(sqle.getMessage());

			return false;
		}

		// success
		return true;
	}

	public static boolean editUser(String userName, String email, String modification) {
		// TODO : edit the user
		return true;
	}

	public static boolean deleteUser(String userName) {
		// TODO : delete the user
		return true;
	}

	// look up user in database
	public boolean queryTeacher(String username) {
		try {
			dbConnection.prepareStatement(DBHelper.USE_POLLSTR).executeUpdate();

			PreparedStatement pst = dbConnection.prepareStatement(DBHelper.SELECT_TEACHER_VALIDATION);
			pst.setString(1, username);
			ResultSet result = pst.executeQuery();

			return result.next();
		} catch (SQLException sqle) {
			System.out.println("DBManager: Error querying for user " + username);
			System.out.println(sqle.getMessage());
			return false;
		}
	}

	// get information for verifying login
	public LoginResponse getTeacherInformation(String username) {

		PreparedStatement pst = null;
		ResultSet result;
		Map<String, String> codeList = new HashMap<String, String>();

		try {
			dbConnection.prepareStatement(DBHelper.USE_POLLSTR).executeUpdate();

			pst = dbConnection.prepareStatement(DBHelper.SELECT_TEACHER_CODE_LIST);
			pst.setString(1, username);
			result = pst.executeQuery();

			while (result.next()) {
				codeList.put(result.getString("code"), result.getString("nickname"));
			}

			pst = dbConnection.prepareStatement(DBHelper.SELECT_TEACHER_DATA);
			pst.setString(1, username);
			result = pst.executeQuery();

			if (result.first()) {
				// user exists in database

				return new LoginResponse(result.getString(2), result.getString(3), result.getString(1),
						codeList, true);

			} else {
				// user is not in database
				return new LoginResponse(false);
			}
		} catch (SQLException sqle) {
			System.out.println("DBManager: Error querying login response for user " + username);
			System.out.println(sqle.getMessage());

			return new LoginResponse(false);
		}
	}

	// validate username and password in database
	public static boolean validateUser(String name, String pass) {
		boolean status = false;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			dbConnection.prepareStatement(DBHelper.USE_POLLSTR).executeUpdate();

			pst = dbConnection.prepareStatement("select * from login where user=? and password=?");
			pst.setString(1, name);
			pst.setString(2, pass);

			rs = pst.executeQuery();

			status = rs.next();

		} catch (SQLException sqle) {
			System.out.println("DBManager: Error validating user " + name);
			System.out.println(sqle.getMessage());
		} finally {
			// clean up connections
			try {
				rs.close();
				pst.close();
				dbConnection.close();
			} catch (SQLException sqle) {
				System.out.println("DBManager: Error closing connections");
				System.out.println(sqle.getMessage());
			}
		}

		return status;
	}

	// create student in database
	public LoginResponse createStudent(LoginRequest login) {
		PreparedStatement pst;
		try {
			dbConnection.prepareStatement(DBHelper.USE_POLLSTR).executeUpdate();

			pst = dbConnection.prepareStatement(DBHelper.INSERT_STUDENT);
			pst.setString(1, login.getUsername());
			pst.setString(2, login.getStudentCode());
			pst.executeUpdate();
			return queryQuizByStudent(getStudentID(login), login.getStudentCode());
		} catch (SQLException sqle) {
			System.out.println("DBManager: Error creating student " + login.getUsername());
			System.out.println(sqle.getMessage());

			return new LoginResponse(false);
		}
	}

	// get student id based on login request object
	public int getStudentID(LoginRequest login) {

		PreparedStatement pst;
		ResultSet results;

		try {
			dbConnection.prepareStatement(DBHelper.USE_POLLSTR).executeUpdate();

			pst = dbConnection.prepareStatement(DBHelper.SELECT_STUDECT_ID);
			pst.setString(1, login.getUsername());
			pst.setString(2, login.getStudentCode());
			results = pst.executeQuery();

			if (results.next()) {
				return results.getInt("id");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return -1;
	}

	// creates a new quiz based on createQuiz object and send a QuizCreated object
	public QuizCreated createQuiz(CreateQuiz quiz) {
		PreparedStatement pst;
		try {
			dbConnection.prepareStatement(DBHelper.USE_POLLSTR).executeUpdate();

			pst = dbConnection.prepareStatement(DBHelper.CREATE_QUIZ);
			pst.setString(1, quiz.getQuizTitle());
			pst.setTimestamp(2, quiz.getStartTime());
			pst.setTimestamp(3, quiz.getEndTime());
			pst.setString(4, quiz.getClassCode());
			pst.executeUpdate();

			for (Map.Entry<String, Answer[]> i : quiz.getQuestions().entrySet()) {
				pst = dbConnection.prepareStatement(DBHelper.CREATE_QUESTION);
				pst.setString(1, i.getKey());
				pst.setString(2, quiz.getQuizTitle());
				pst.setTimestamp(3, quiz.getStartTime());
				pst.setTimestamp(4, quiz.getEndTime());
				pst.setString(5, quiz.getClassCode());
				pst.setInt(6, quiz.getQuizType().ordinal() + 1);
				pst.executeUpdate();

				for (Answer k : i.getValue()) {
					pst = dbConnection.prepareStatement(DBHelper.CREATE_ANSWER);
					pst.setString(1, k.getAnswer());
					pst.setBoolean(2, k.getCorrectness());
					pst.setString(3, i.getKey());
					pst.setString(4, quiz.getQuizTitle());
					pst.setTimestamp(5, quiz.getStartTime());
					pst.setTimestamp(6, quiz.getEndTime());
					pst.setString(7, quiz.getClassCode());
					pst.setInt(8, quiz.getQuizType().ordinal() + 1);
					pst.executeUpdate();
				}
			}

			return new QuizCreated(true, "");
		} catch (SQLException sqle) {
			System.out.println("DBManager: Error creating quiz " + quiz.getQuizTitle());
			System.out.println(sqle.getMessage());
			sqle.printStackTrace();

			return new QuizCreated(false, "Cannot create quiz");
		}
	}

	// TODO : edit the quiz
	public static boolean editQuiz(String quizName, String url, String time) {
		return true;
	}

	// TODO : delete the quiz
	public static boolean deleteQuiz(String quizName, String url) {
		return true;
	}

	// find quizzes to show the student
	public LoginResponse queryQuizByStudent(int studentID, String classCode) {
		PreparedStatement pst;
		ResultSet rst;
		ArrayList<Integer> quizIDs = new ArrayList<>();
		ArrayList<String> quizNames = new ArrayList<>();

		try {
			dbConnection.prepareStatement(DBHelper.USE_POLLSTR).executeUpdate();

			pst = dbConnection.prepareStatement(DBHelper.SELECT_STUDENT_QUIZZES);
			pst.setString(1, classCode);
			rst = pst.executeQuery();

			while (rst.next()) {
				quizIDs.add(rst.getInt(1));
				quizNames.add(rst.getString(2));
			}

			return new LoginResponse(studentID, quizIDs.toArray(new Integer[0]), quizNames.toArray(new String[0]),
					true);
		} catch (SQLException sqle) {
			sqle.printStackTrace();
			return new LoginResponse(false);
		}

	}

	// find if a student exists already
	public boolean queryStudentHasPreviouslyLogedIn(String username, String classCode) {
		try {
			dbConnection.prepareStatement(DBHelper.USE_POLLSTR).executeUpdate();

			PreparedStatement pst = dbConnection.prepareStatement(DBHelper.SELECT_STUDENT_USERNAME);
			pst.setString(1, username);
			pst.setString(2, classCode);

			ResultSet result = pst.executeQuery();

			// TODO: need to check for finding multiple usernames?
			if (!result.first()) {
				System.out.println("DBManager: Student username " + username + " not found");

				return false;
			}
		} catch (SQLException sqle) {
			System.out.println("DBManager: Error querying student username " + username);
			System.out.println(sqle.getMessage());

			return false;
		}

		return true;
	}

	// find class code for student
	public boolean queryClassCode(String code) {

		try {
			dbConnection.prepareStatement(DBHelper.USE_POLLSTR).executeUpdate();

			PreparedStatement pst = dbConnection.prepareStatement(DBHelper.SELECT_CLASS_CODE);
			pst.setString(1, code);

			ResultSet result = pst.executeQuery();

			if (!result.first()) {
				return false;
				// System.out.println("DBManager: Student class code for username " + username +
				// " not found");
			} else {
				// found class code
				// classCode = result.getString(1);
				return true;
			}
		} catch (SQLException sqle) {
			System.out.println("DBManager: Error querying student class code " + code);
			System.out.println(sqle.getMessage());
		}

		return false;
	}

	// create a class code for teacher
	public boolean createClassCode(NewClassCode code) {
		try {
			dbConnection.prepareStatement(DBHelper.USE_POLLSTR).executeUpdate();

			PreparedStatement pst = dbConnection.prepareStatement(DBHelper.INSERT_CODE);
			pst.setString(1, code.getCode());
			pst.setString(2, code.getUsername());

			pst.executeUpdate();

			// found class code
			// classCode = result.getString(1);
			System.out.println("DBManager: 2 Class code created for code " + code.getCode() + " for username: "
					+ code.getUsername());

			return true;

		} catch (SQLException sqle) {
			System.out.println("DBManager: 3 Error creating class code " + code.getCode() + " for username: "
					+ code.getUsername());
			System.out.println(sqle.getMessage());
		}

		System.out.println(
				"DBManager: 2 Class code created for code " + code.getCode() + " for username: " + code.getUsername());
		return false;
	}

	public ClassCodeAdded addClassCodeName(AssignClassNames names) {

		PreparedStatement pst;

		try {
			dbConnection.prepareStatement(DBHelper.USE_POLLSTR).executeUpdate();

			for (int i = 0; i < names.getCodes().length; ++i) {
				pst = dbConnection.prepareStatement(DBHelper.EDIT_CODE_NICKNAME_BY_CODE);
				pst.setString(1, names.getNames()[i]);
				pst.setString(2, names.getCodes()[i]);
				pst.executeUpdate();
			}

			return new ClassCodeAdded(true);

		} catch (SQLException e) {
			e.printStackTrace();
			return new ClassCodeAdded(false);
		}
	}

	// adds student answers based on StrudentAnswers object and send a answers added
	// object
	public AnswersAdded EnterStudentAnswer(StudentAnswers answer) {

		PreparedStatement pst;

		try {
			dbConnection.prepareStatement(DBHelper.USE_POLLSTR).executeUpdate();

			for (Entry<String, String> iter : answer.getQuestionsAndAnswers().entrySet()){
				pst = dbConnection.prepareStatement(DBHelper.SELECT_ANSWER_USING_QUIZ_AND_QUESTION_IDS);
				pst.setString(1, iter.getKey());
				pst.setInt(2, answer.getQuizID());

				if (pst.executeQuery().first()) {
					pst.close();
					pst = dbConnection.prepareStatement(DBHelper.INSERT_STUDENT_ANSWER);
					pst.setInt(1, answer.getQuizID());
					pst.setString(2, iter.getKey());
					pst.setString(3, iter.getValue());
					pst.setInt(4, answer.getStudentID());
					pst.executeUpdate();
					pst.close();
				} else {
					pst.close();
					pst = dbConnection.prepareStatement(DBHelper.INSERT_STUDENT_SHORT_ANSWER);
					pst.setString(1, iter.getValue());
					pst.setInt(2, answer.getQuizID());
					pst.setString(3, iter.getKey());
					pst.setInt(4, answer.getStudentID());
					pst.executeUpdate();
					pst.close();
				}
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new AnswersAdded(false);
		}

		return new AnswersAdded(true);
	}

	// find answers to quiz
	public QuizContents quizContents(FetchQuizWithID quiz) {
		Map<String, String[]> info = new HashMap<>();
		Types questionType = Types.TRUE_FALSE;

		PreparedStatement pst;
		ResultSet result, results2;
		ArrayList<String> answers = new ArrayList<>();

		try {
			dbConnection.prepareStatement(DBHelper.USE_POLLSTR).executeUpdate();

			pst = dbConnection.prepareStatement(DBHelper.SELECT_QUESTION_BY_QUIZ_ID);
			pst.setInt(1, quiz.getID());

			result = pst.executeQuery();

			while (result.next()) {
				questionType = Types.values()[result.getInt("fk_question_type") - 1];
				pst = dbConnection.prepareStatement(DBHelper.SELECT_ANSWERS_BY_QUESION);
				pst.setInt(1, result.getInt("id"));
				results2 = pst.executeQuery();
				while (results2.next()) {
					answers.add(results2.getString(1));
				}
				info.put(result.getString("question"), answers.toArray(new String[0]));
				answers.clear();
			}

		} catch (SQLException sqle) {
			System.out.println("DBManager: Error fetching quiz information for id " + quiz.getID());
			System.out.println(sqle.getMessage());
			return new QuizContents(null, questionType);
		}

		System.out.println("DBManager: Quiz information " + quiz.getID() + " was successfully retrived.");
		return new QuizContents(info, questionType);
	}

	// query db for admin based on username and respond back with a object
	public LoginResponse queryAdmin(String username) {
		PreparedStatement pst = null;
		ResultSet result;
		String firstName = "", lastName = "", password = "";

		try {
			dbConnection.prepareStatement(DBHelper.USE_POLLSTR).executeUpdate();

			pst = dbConnection.prepareStatement(DBHelper.SELECT_ADMIN_DATA);
			pst.setString(1, username);
			result = pst.executeQuery();

			if (result.first()) {
				firstName = result.getString("firstname");
				lastName = result.getString("lastname");
				password = result.getString("password");

				System.out.println("DBManager: " + firstName + " " + lastName + " " + password);
				// user exists in database
				return new LoginResponse(firstName, lastName, password, true);

			} else {
				System.out.println("DBManager: Error querying login response for user " + username);
				// user is not in database
				return new LoginResponse(false);
			}
		} catch (SQLException sqle) {
			System.out.println("DBManager: Error querying login response for user " + username);
			System.out.println(sqle.getMessage());

			return new LoginResponse(false);
		}
	}

	// sends result sets on teacher & class code list query for admin manage display
	public DatabaseData getDatabaseData() {
		PreparedStatement pst = null;
		ResultSet teachersResults = null;
		ResultSet codeListResults = null;
		try {
			dbConnection.prepareStatement(DBHelper.USE_POLLSTR).executeUpdate();

			pst = dbConnection.prepareStatement(DBHelper.SELECT_ALL_TEACHERS_DATA);
			teachersResults = pst.executeQuery();

			pst = dbConnection.prepareStatement(DBHelper.SELECT_ALL_CLASS_CODE);
			codeListResults = pst.executeQuery();

			if (teachersResults.first() && codeListResults.first()) {

				// user exists in database
				return new DatabaseData(teachersResults, codeListResults);

			} else {
				System.out.println("DBManager: Error querying data for teachers and codelist.");
				// user is not in database
				return new DatabaseData(null, null);
			}
		} catch (SQLException sqle) {
			System.out.println("DBManager: Error querying data for teachers and codelist.");
			System.out.println(sqle.getMessage());

			return new DatabaseData(null, null);
		}
	}

	@SuppressWarnings({ "unchecked", "unused" })
	public TeacherDatabaseUpdated updateDatabaseForTeacher(UpdateTeachersDatabase teacherInformation) {

		JSONObject toUpdate = teacherInformation.getToUpdate(), toEdit;
		PreparedStatement pst;
		String deleteQuery;

		try {

			dbConnection.prepareStatement(DBHelper.USE_POLLSTR).executeUpdate();
			if (toUpdate.containsKey("Edit")) {
				for(Entry<String, JSONArray> var : ((Map<String, JSONArray>) toUpdate.get("Edit")).entrySet()) {
					switch (var.getKey()) {
					case "class":
						for (Object i: var.getValue()) {
							if (i instanceof JSONObject) {
								toEdit = (JSONObject)i;
								pst = dbConnection.prepareStatement(DBHelper.EDIT_CODE_NICKNAME_BY_ID);
								pst.setString(1, (String)toEdit.get("nickname"));
								pst.setInt(2, Integer.parseInt((String)toEdit.get("id")));
								pst.executeUpdate();
							}
						}
						break;
					case "quiz":
						for (Object i: var.getValue()) {
							if (i instanceof JSONObject) {
								toEdit = (JSONObject)i;
								pst = dbConnection.prepareStatement(DBHelper.EDIT_QUIZ_BY_ID);
								pst.setString(1, (String)toEdit.get("title"));
								pst.setTimestamp(2, Timestamp.valueOf((String)toEdit.get("starttime")));
								pst.setTimestamp(3, Timestamp.valueOf((String)toEdit.get("endtime")));
								pst.setInt(4, Integer.parseInt((String)toEdit.get("id")));
								pst.executeUpdate();
							}
						}
						break;
					case "question":
						for (Object i: var.getValue()) {
							if (i instanceof JSONObject) {
								toEdit = (JSONObject)i;
								pst = dbConnection.prepareStatement(DBHelper.EDIT_QUESTION_BY_ID);
								pst.setString(1, (String)toEdit.get("question"));
								pst.setInt(2, Integer.parseInt((String)toEdit.get("id")));
								pst.executeUpdate();
							}
						}
						break;
					case "answer":
						for (Object i: var.getValue()) {
							if (i instanceof JSONObject) {
								toEdit = (JSONObject)i;
								pst = dbConnection.prepareStatement(DBHelper.EDIT_ANSWER_BY_ID);
								pst.setString(1, (String)toEdit.get("answer"));
								pst.setBoolean(2, (boolean)toEdit.get("correct"));
								pst.setInt(3, Integer.parseInt((String)toEdit.get("id")) );
								pst.executeUpdate();
							}
						}

					}
				}
			}

			if (toUpdate.containsKey("Delete")) {
				for(Entry<String, JSONArray> var : ((Map<String, JSONArray>) toUpdate.get("Delete")).entrySet()) {
					switch (var.getKey()) {
					case "class":
						deleteQuery = DBHelper.DELETE_CLASS_CODE_USING_LIST;
						for (Object i : var.getValue().toArray())
							deleteQuery += "?,";
						pst = dbConnection.prepareStatement(deleteQuery.substring(0, deleteQuery.length()-1) + ");");
						for (int i = 0; i < var.getValue().size(); ++i)
							pst.setInt(i+1, Integer.parseInt((String)var.getValue().get(i)));
						pst.executeUpdate();
						break;
					case "quiz":
						deleteQuery = DBHelper.DELETE_QUIZ_USING_LIST;
						for (Object i : var.getValue().toArray())
							deleteQuery += "?,";
						pst = dbConnection.prepareStatement(deleteQuery.substring(0, deleteQuery.length()-1) + ");");
						for (int i = 0; i < var.getValue().size(); ++i)
							pst.setInt(i+1, Integer.parseInt((String)var.getValue().get(i)));
						pst.executeUpdate();
						break;
					case "question":
						deleteQuery = DBHelper.DELETE_QUESTION_USING_LIST;
						for (Object i : var.getValue().toArray())
							deleteQuery += "?,";
						pst = dbConnection.prepareStatement(deleteQuery.substring(0, deleteQuery.length()-1) + ");");
						for (int i = 0; i < var.getValue().size(); ++i)
							pst.setInt(i+1, Integer.parseInt((String)var.getValue().get(i)));
						pst.executeUpdate();
						break;
					case "answer":
						deleteQuery = DBHelper.DELETE_ANSWER_USING_LIST;
						for (Object i : var.getValue().toArray())
							deleteQuery += "?,";
						pst = dbConnection.prepareStatement(deleteQuery.substring(0, deleteQuery.length()-1) + ");");
						for (int i = 0; i < var.getValue().size(); ++i)
							pst.setInt(i+1, Integer.parseInt((String)var.getValue().get(i)));
						pst.executeUpdate();
						break;
					}
				}
			}
			
			return new TeacherDatabaseUpdated(getTeacherData((String) toUpdate.get("username")).getTeacherData(), true);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	// Takes all the data from the database related to a certain teacher and
	// delivers a JSON object
	@SuppressWarnings("unchecked")
	public TeachersData getTeacherData(String teacherData) {
		int teacherId = 0, 
				classCodeId = 0, 
				studentId = 0, 
				quizId = 0, 
				questionId = 0, 
				answerId = 0;

		PreparedStatement pst = null;
		ResultSet results = null, 
				classInfo = null, 
				studentInfo = null, 
				studentAnswers = null, 
				quizInfo = null,
				questionInfo = null, 
				answerInfo = null, 
				temp = null;

		JSONObject mainObj = new JSONObject(),
				studentObj = new JSONObject(),
				answerObj = new JSONObject(),
				questionObj = new JSONObject(),
				quizObj = new JSONObject(),
				classObj = new JSONObject();

		JSONArray classArry = new JSONArray(),
				studentArry = new JSONArray(),
				quizArray = new JSONArray(),
				answerArr = new JSONArray(),
				questionArr = new JSONArray();

		try {
			dbConnection.prepareStatement(DBHelper.USE_POLLSTR).executeUpdate();

			pst = dbConnection.prepareStatement(DBHelper.SELECT_TEACHER_ID);
			pst.setString(1, teacherData);

			results = pst.executeQuery();
			if (results.first()) {
				teacherId = results.getInt("id");

				pst = dbConnection.prepareStatement(DBHelper.SELECT_CLASS_CODE_INFO);
				pst.setInt(1, teacherId);

				classInfo = pst.executeQuery();

				while (classInfo.next()) {

					System.out.println(classInfo.getString("nickname") + " " + classInfo.getString("code") + " " + classInfo.getString("created") + " " + classInfo.getInt("id"));

					classObj.put("id", classCodeId = classInfo.getInt("id"));
					classObj.put("nickname", classInfo.getString("nickname"));
					classObj.put("code", classInfo.getString("code"));
					classObj.put("created", classInfo.getString("created"));

					pst = dbConnection.prepareStatement(DBHelper.SELECT_STUDENT_INFO_BY_CLASS_CODE);
					pst.setInt(1, classCodeId);

					studentInfo = pst.executeQuery();

					while(studentInfo.next()) {

						studentObj.put("id", studentId = studentInfo.getInt("id"));
						studentObj.put("username", studentInfo.getString("username"));

						System.out.println(studentInfo.getInt("id") + " " + studentInfo.getString("username"));

						// Producing the sum of the marks the student has
						pst = dbConnection.prepareStatement(DBHelper.SELECT_STUDENT_GRADES);
						pst.setInt(1, studentId);

						if ((studentAnswers = pst.executeQuery()).first()) {
							// Using Object instead of Integer to protect the code in the cass of a null value
							studentObj.put("CorrectAnswers", studentAnswers.getObject("correctAnswers"));
							studentObj.put("WrongAnswers", studentAnswers.getObject("wrongAnswers"));
						}

						studentArry.add(studentObj.clone());
						studentObj.clear();
					}

					classObj.put("students", studentArry.clone());

					studentArry.clear();


					pst = dbConnection.prepareStatement(DBHelper.SELECT_QUIZ_BY_CLASS_CODE_ID);
					pst.setInt(1, classCodeId);

					quizInfo = pst.executeQuery();
					while(quizInfo.next()) {

						quizObj.put("id", quizId = quizInfo.getInt("id"));
						quizObj.put("title", quizInfo.getString("title"));
						quizObj.put("start_time", quizInfo.getString("start_time"));
						quizObj.put("end_time", quizInfo.getString("end_time"));
						quizObj.put("created", quizInfo.getString("created"));

						pst = dbConnection.prepareStatement(DBHelper.SELECT_QUESTION_FOR_QUIZ_ID);
						pst.setInt(1, quizId);

						questionInfo = pst.executeQuery();

						while(questionInfo.next()) {

							questionObj.put("id", questionId = questionInfo.getInt("id"));
							questionObj.put("question", questionInfo.getString("question"));
							questionObj.put("fk_question_type", questionInfo.getInt("fk_question_type"));

							pst = dbConnection.prepareStatement(DBHelper.SELECT_ANSWERS_BY_QUESTION_ID);
							pst.setInt(1, questionId);

							answerInfo = pst.executeQuery();
							while (answerInfo.next()) {
								answerObj.put("id", answerId = answerInfo.getInt("id"));
								answerObj.put("answer", answerInfo.getString("answer"));
								answerObj.put("correct", answerInfo.getBoolean("correct"));

								pst = dbConnection.prepareStatement(DBHelper.SELECT_COUNT_STUDENTS_WHO_SELECTED_ANSWER);
								pst.setInt(1, answerId);

								if ((temp = pst.executeQuery()).first())
									answerObj.put("countSelected", temp.getInt("total"));

								pst = dbConnection.prepareStatement(DBHelper.SELECT_STUDENTS_WHO_SELECTED_ANSWER);
								pst.setInt(1, answerId);

								ArrayList<String> names = new ArrayList<>();
								for (temp = pst.executeQuery(); temp.next();) {
									names.add(temp.getString("username"));
								}

								if (!names.isEmpty())
									answerObj.put("Students", names);

								answerArr.add(answerObj.clone());
								answerObj.clear();
							}

							answerInfo.close();

							if (answerArr.isEmpty()) {
								pst = dbConnection
										.prepareStatement(DBHelper.SELECT_STUDENT_SHORT_ANSWERS_BY_QUESTION_ID);
								pst.setInt(1, questionId);

								for (answerInfo = pst.executeQuery(); answerInfo.next();) {
									answerObj.put("answer", answerInfo.getString("short_answer"));
									answerObj.put("name", answerInfo.getString("username"));
									answerArr.add(answerObj.clone());
									answerObj.clear();
								}

								answerInfo.close();
							}

							questionObj.put("answers", answerArr.clone());
							answerArr.clear();

							questionArr.add(questionObj.clone());
							questionObj.clear();
						}

						quizObj.put("questions", questionArr.clone());
						questionArr.clear();

						quizArray.add(quizObj.clone());
						quizObj.clear();
					}

					classObj.put("quizzes", quizArray.clone());
					quizArray.clear();

					classArry.add((JSONObject) classObj.clone());
					classObj.clear();

				}

				mainObj.put("classes", classArry);

			}
		} catch (SQLException sqle) {
			System.out.println("DBManager: Error querying data for teacher " + teacherData);
			System.out.println(sqle.getMessage());

			return null;
		}
		return new TeachersData(mainObj);
	}
}
