/*
Author: 	Jinesh Bhatt
Author:		Silviu Riley	
Class:		Web Enterprise Applications 
Project:	Pollster Assignment 4 Version-0.3
Teacher: 	Douglas King
Date:		2018/03/28
File:		Server.java
Purpose:	Manage connection to Model and handle them in threads
*/
package model;

import java.io.IOException;
import java.lang.String;
import java.net.ServerSocket;
import library.ControllerModelLibrary;

public class Server {
	// Network info
	private final static int DEFAULT_PORT = ControllerModelLibrary.MODEL_PORT;
	private final static String DEFAULT_IP = ControllerModelLibrary.MODEL_ADDRESS;
	private final static int MIN_PORT = 1024;
	private final static int MAX_PORT = 65535;

	public static void main(String[] args) {
		int port = DEFAULT_PORT;

		// Start the server at user defined port if passed in
		if (args.length == 1 && args[0].matches("\\d+")) {
			port = Integer.parseInt(args[0]);

			// Use default port if improper number passed in
			if (port < MIN_PORT || port > MAX_PORT) {
				port = DEFAULT_PORT;
			}
		}

		ServerSocket socket = null;

		// Create a new server socket with port number,
		// in an infinite loop accept connection
		// launch a new thread and pass the socket instance
		// Listen for incoming connections and respond to them
		try {
			System.out.println("Server: Waiting for Controller at " + DEFAULT_IP + ":" + port);

			socket = new ServerSocket(port);

			while (true) {
				new Thread(new ServerSocketRunnable(socket.accept())).start();
			}

		} catch (IOException ioe) {
			System.out.println("Server: Connection at " + port + " could not be established");
			System.out.println(ioe.getMessage());
			ioe.printStackTrace();
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {

				}
			}
		}
	}
}