/*
Author: 	Jinesh Bhatt
Author:		Silviu Riley
Author:		Joshua Muysson
Class:		Web Enterprise Applications 
Project:	Pollster Assignment 4 Version-0.3
Teacher: 	Douglas King
Date:		2018-03-28
File:		DBHelper.java
Purpose:	Holds template strings for DBManager.java to use when executing/creating queries
*/
package library;

import library.ControllerModelLibrary.SignUp;

public class DBHelper {
	// Query to create database
	public final static String CREATE_DB_POLLSTR = "create database if not exists pollstr default character set utf8;";
	
	// Query to use the created database
	public final static String USE_POLLSTR = "use pollstr;";
	
	// Queries to create tables
	public final static String CREATE_TBL_TEACHERS = "create table if not exists teachers (id int unsigned not null auto_increment, username text not null, email text not null, firstname text not null, lastname text not null, password binary(60) not null, created datetime not null default now(), validated boolean default 0, primary key (id));";
	
	public final static String CREATE_TBL_ADMINS = "create table if not exists admin (id int unsigned not null auto_increment, username text not null, email text not null, firstname text not null, lastname text not null, password binary(60) not null, created datetime not null default now(), primary key (id));";
		
	public final static String CREATE_TBL_CLASS_CODES = "create table if not exists class_codes (id int unsigned not null auto_increment, nickname varchar(255), code varchar(10) not null UNIQUE, created datetime not null default now(), fk_teacher int unsigned not null, primary key (id), foreign key (fk_teacher) references teachers(id) on delete cascade);";
	
	public final static String CREATE_TBL_STUDENTS = "create table if not exists students (id int unsigned not null auto_increment, username text not null, created datetime not null default now(), fk_class_code int unsigned not null, primary key (id), foreign key (fk_class_code) references class_codes(id) on delete cascade);";
	
	public final static String CREATE_TBL_QUIZZES = "create table if not exists quizzes (id int unsigned not null auto_increment, title text not null, start_time datetime not null, end_time datetime not null, created datetime not null default now(), fk_class_code int unsigned not null, primary key (id), foreign key (fk_class_code) references class_codes(id) on delete cascade);";
	
	public final static String CREATE_TBL_QUESTION_TYPES = "create table if not exists question_types (id int unsigned not null auto_increment, type text not null, primary key (id));";
			
	public final static String CREATE_TBL_QUESTIONS = "create table if not exists questions (id int unsigned not null auto_increment, question text not null, fk_quiz int unsigned not null, fk_question_type int unsigned not null, primary key (id), foreign key (fk_quiz) references quizzes(id) on delete cascade, foreign key (fk_question_type) references question_types(id) on delete cascade);";
	
	public final static String CREATE_TBL_ANSWERS = "create table if not exists answers (id int unsigned not null auto_increment, answer text not null, correct boolean, fk_question int unsigned not null, primary key (id), foreign key (fk_question) references questions(id) on delete cascade);";
	
	public final static String CREATE_TBL_STUDENT_ANSWERS = "create table if not exists student_answers (id int unsigned not null auto_increment, short_answer text, fk_student int unsigned not null, fk_answer int unsigned, fk_question int unsigned, primary key (id), foreign key (fk_student) references students(id) on delete cascade, foreign key (fk_answer) references answers(id) on delete cascade, foreign key (fk_question) references questions(id) on delete cascade);";
	
	/* ------------------------- Automatically insert certain data DB statements -------------------------------------------- */
	
	public final static String INSERT_QUIZTYPE_MULTIPLE = "insert into question_types (id, type) values (1, \"Multiple choice\");";
	
	public final static String INSERT_QUIZTYPE_TRUEFALSE = "insert into question_types (id, type) values (2, \"True or False\");";
	
	public final static String INSERT_QUIZTYPE_SHORT = "insert into question_types (id, type) values (3, \"Short Answer\");";

	// Data to insert into Teachers (for testing)
	public final static SignUp[] TEACHER_DATA = {
			new SignUp("abeswetherick1", "abeswetherick1@qq.com", BCrypt.hashpw("Fr8jcj", BCrypt.gensalt()), "Abelard", "Beswetherick", ControllerModelLibrary.Permission.TEACHER),
			new SignUp("kgeeves6", "kgeeves6@imageshack.us", BCrypt.hashpw("2J7SMd3oK", BCrypt.gensalt()), "Kerr", "Geeves", ControllerModelLibrary.Permission.TEACHER),
			new SignUp("sfoale9", "sfoale9@fema.gov", BCrypt.hashpw("eKFkoHqsr", BCrypt.gensalt()), "Salmon", "Foale", ControllerModelLibrary.Permission.TEACHER),
			new SignUp("cprescotea", "cprescotea@princeton.edu", BCrypt.hashpw("24gVywg9WL", BCrypt.gensalt()), "Celina", "Prescote", ControllerModelLibrary.Permission.TEACHER),
			new SignUp("tlarventb", "tlarventb@php.net", BCrypt.hashpw("pmZDiDk6j42Y", BCrypt.gensalt()), "Tiffany", "Larvent", ControllerModelLibrary.Permission.TEACHER),
			new SignUp("bpetrosellid", "bpetrosellid@opensource.org", BCrypt.hashpw("ea45Ko", BCrypt.gensalt()), "Blinnie", "Petroselli", ControllerModelLibrary.Permission.TEACHER),
			new SignUp("dwhorltong", "dwhorltong@icio.us", BCrypt.hashpw("6DEjRaV", BCrypt.gensalt()), "Dulcy", "Whorlton", ControllerModelLibrary.Permission.TEACHER),
			new SignUp("vflanaganj", "vflanaganj@phpbb.com", BCrypt.hashpw("eMGOVT", BCrypt.gensalt()), "Vevay", "Flanagan", ControllerModelLibrary.Permission.TEACHER),
			new SignUp("cedgink", "cedgink@columbia.edu", BCrypt.hashpw("0dH91Z", BCrypt.gensalt()), "Claire", "Edgin", ControllerModelLibrary.Permission.TEACHER),
			new SignUp("bmatantsevo", "bmatantsevo@mozilla.com", BCrypt.hashpw("l2cnQwziH", BCrypt.gensalt()), "Bronnie", "Matantsev", ControllerModelLibrary.Permission.TEACHER),
			new SignUp("lrouchys", "lrouchys@tripadvisor.com", BCrypt.hashpw("DapIkgbGi", BCrypt.gensalt()), "Leicester", "Rouchy", ControllerModelLibrary.Permission.TEACHER),
			new SignUp("ecoxwellt", "ecoxwellt@ezinearticles.com", BCrypt.hashpw("mUt96V1", BCrypt.gensalt()), "Eachelle", "Coxwell", ControllerModelLibrary.Permission.TEACHER),
			new SignUp("mkalvinw", "mkalvinw@utexas.edu", BCrypt.hashpw("PkdQQoKN3ue", BCrypt.gensalt()), "Maryjo", "Kalvin", ControllerModelLibrary.Permission.TEACHER),
			new SignUp("boldlandy", "boldlandy@home.pl", BCrypt.hashpw("NaIiTYMSo0A", BCrypt.gensalt()), "Bryn", "Oldland", ControllerModelLibrary.Permission.TEACHER),
			new SignUp("tlillicrop13", "tlillicrop13@ed.gov", BCrypt.hashpw("bLG15kX", BCrypt.gensalt()), "Theo", "Lillicrop", ControllerModelLibrary.Permission.TEACHER),
			new SignUp("mbuy18", "mbuy18@freewebs.com", BCrypt.hashpw("g1dqmC7nUy", BCrypt.gensalt()), "Marianne", "Buy", ControllerModelLibrary.Permission.TEACHER),
			new SignUp("test", "hkleinert19@delicious.com", BCrypt.hashpw("vQdjJ1wE26aM", BCrypt.gensalt()), "Hedwig", "Kleinert", ControllerModelLibrary.Permission.TEACHER),
			new SignUp("temp", "john@doe.com", BCrypt.hashpw("password", BCrypt.gensalt()), "John", "Doe", ControllerModelLibrary.Permission.TEACHER)		
	};
	
	// Data to insert into Admin (for testing)
	public final static SignUp[] ADMIN_DATA = {
			new SignUp("jinesh", "jinesh@root.com", BCrypt.hashpw("password", BCrypt.gensalt()), "Jinesh", "B", ControllerModelLibrary.Permission.ADMIN),
			new SignUp("joshua", "joshua@root.com", BCrypt.hashpw("password", BCrypt.gensalt()), "Joshua", "M", ControllerModelLibrary.Permission.ADMIN),
			new SignUp("silivu", "silivu@root.com", BCrypt.hashpw("password", BCrypt.gensalt()), "Silivu", "R", ControllerModelLibrary.Permission.ADMIN),
			new SignUp("arjun", "arjun@root.com", BCrypt.hashpw("password", BCrypt.gensalt()), "Arjun", "S", ControllerModelLibrary.Permission.ADMIN),
			new SignUp("gianpiero", "gianpiero@root.ca", BCrypt.hashpw("password", BCrypt.gensalt()), "Gianpiero", "P", ControllerModelLibrary.Permission.ADMIN)
	};
	
	/* ------------------------- Start of teacher DB statements -------------------------------------------- */
		
	// TODO: Add teacher db statements	
	public final static String INSERT_TEACHER = "insert into teachers (username, firstname, lastname, email, password) values (?, ?, ?, ?, ?);";
	
	// TODO: edit teacher db statements
	public final static String UPDATE_TEACHER_VAIDATION = "update teachers set validated=1 where username=?";
	
	// TODO: delete teacher db statements
	public final static String DELETE_TEACHER_BY_USERNAME = "delete from teachers where username=?;";
	
	// TODO: query teacher db statements
	
	public final static String SELECT_ALL_TEACHERS_DATA = "select * from teachers;";
	
	public final static String SELECT_TEACHER_DATA = "select password, firstname, lastname from teachers where username=?;";
	
	public final static String SELECT_TEACHER_CODE_LIST ="select code, nickname from class_codes inner join teachers where fk_teacher=teachers.id and username=?;";
	
	public final static String SELECT_TEACHER_USERNAME = "select username from teachers where username=?;";
	
	public final static String SELECT_TEACHER_ID = "select id from teachers where username=?;";

	public final static String SELECT_TEACHER_VALIDATION = "select validated from teachers where username=? and validated=1";
	
	/* ------------------------- End of teacher DB statements -------------------------------------------- */
	
	/* ------------------------- Start of admin DB statements -------------------------------------------- */
	
	// TODO: Add admin db statements	
	public final static String INSERT_ADMIN = "insert into admin (username, firstname, lastname, email, password) values (?, ?, ?, ?, ?);";
	
	// TODO: edit admin db statements
	// TODO: delete admin db statements
		
	// TODO: query admin db statements

	public final static String SELECT_ADMIN_USERNAME = "select username from admin where username=?;";
	
	public final static String SELECT_ADMIN_DATA = "select firstname, lastname, password from admin where username=?;";
	
	/* ------------------------- End of admin DB statements -------------------------------------------- */
	
	/* ------------------------- Start of student DB statements ------------------------------------------ */
	// TODO: add student db statements
	
	public final static String INSERT_STUDENT ="insert into students (username, fk_class_code) value(?, (select class_codes.id from class_codes where code=?));";
	
	public final static String INSERT_STUDENT_ANSWER = "insert into student_answers set fk_answer=(select a.id from answers as a inner join questions as qs inner join quizzes as qz where a.fk_question=qs.id and qs.fk_quiz=qz.id and qz.id=? and qs.question=? and a.answer=?), fk_student=?;";
	
	public final static String INSERT_STUDENT_SHORT_ANSWER = "insert into student_answers set short_answer=?, fk_question=(select qs.id from questions as qs inner join quizzes as qz where qs.fk_quiz=qz.id and qz.id=? and qs.question=?), fk_student=?;";
	// TODO: edit student db statements
	// TODO: delete student db statements
	// TODO: query student information
	
	public final static String SELECT_STUDENT_USERNAME = "select username from students where username=? and fk_class_code=(select id from class_codes where code=?);";
	
	public final static String SELECT_STUDECT_ID = "select students.id from students inner join class_codes as cc where cc.id=students.fk_class_code and students.username=? and cc.code=?";
	
	public final static String SELECT_STUDENT_QUIZZES = "select qz.id, qz.title from quizzes as qz inner join class_codes as cc on qz.fk_class_code=cc.id where cc.code=? and qz.start_time <= now() and qz.end_time >= now();";
	
	public final static String SELECT_STUDENT_INFO_BY_CLASS_CODE = "SELECT id, username FROM students where fk_class_code=?;";
	
	public final static String SELECT_STUDENT_ANSWERS = "select * from student_answers where fk_student=?;";
	
	public final static String SELECT_STUDENT_GRADES = "select sum(correct) as correctAnswers, count(a.id) - sum(correct) as wrongAnswers from answers as a inner join student_answers as sa on sa.fk_answer=a.id where sa.fk_student=?;";
	/* ------------------------- End of student DB statements -------------------------------------------- */
	
	/* ------------------------- Start of class DB statements -------------------------------------------- */
	
	// TODO: Add class codes
	public final static String INSERT_CODE = "insert into class_codes (code, fk_teacher) value(?, (select id from teachers where username=?));";
	
	// TODO: edit class codes db statements
	public final static String EDIT_CODE_NICKNAME_BY_CODE = "update classe_codes set nickname=? where code=?";
	
	public final static String EDIT_CODE_NICKNAME_BY_ID = "update classe_codes set nickname=? where id=?";
	
	// TODO: delete class codes db statements
	public final static String DELETE_CLASS_CODE_USING_LIST = "delete from class_codes where id in (";
	
	// TODO: query class codes information
	public final static String SELECT_ALL_CLASS_CODE = "select * from class_codes;";
	
	public final static String SELECT_CLASS_CODE = "select code from class_codes where code=?;";
	
	public final static String SELECT_CLASS_CODE_ID = "select class_codes.id from class_codes where code=?;";
	
	public final static String SELECT_CLASS_CODE_INFO = "select id, nickname, code, created from class_codes where fk_teacher=?;";
	
	/* ------------------------- End of class DB statements ---------------------------------------------- */
	
	/* ------------------------- Start of quiz DB statements --------------------------------------------- */
	
	// TODO: Query quiz db statements
	public final static String SELECT_QUIZ_TITLE_AND_TYPE_BY_ID = "select title, fk_question_type from quizzes inner join questions where quizzes.id=fk_quiz and (quizzes.id=?);";
	
	public final static String SELECT_QUESTION_BY_QUIZ_ID = "select questions.id, question, fk_question_type from questions inner join quizzes where quizzes.id=fk_quiz and (quizzes.id=?);";
	
	public final static String SELECT_ANSWERS_BY_QUESION = "select answer from answers where fk_question=?;";
	
	public final static String SELECT_ANSWERS_BY_QUESTION_ID = "select id, answer, correct from answers where fk_question=?;";
	
	public final static String SELECT_ANSWER_USING_QUIZ_AND_QUESTION_IDS = "select answers.id from answers inner join questions as q on q.id=answers.fk_question where q.question=? and q.fk_quiz=?";
	
	public final static String SELECT_LIST_OF_QUESTION_ID = "select questions.id from questions inner join quizzes where fk_quiz=quizzes.id and quizzes.fk_class_code=?;";
	
	public final static String SELECT_QUIZ_BY_CLASS_CODE_ID = "select id, title, start_time, end_time, created from quizzes where fk_class_code=?;";
	
	public final static String SELECT_QUESTION_FOR_QUIZ_ID = "select id, question, fk_question_type from questions where fk_quiz=?;";
	
	public final static String SELECT_COUNT_STUDENTS_WHO_SELECTED_ANSWER = "select count(*) as total from answers as a inner join student_answers as sa on a.id=sa.fk_answer where a.id=?;";
	
	public final static String SELECT_STUDENT_SHORT_ANSWERS_BY_QUESTION_ID = "select username, short_answer from student_answers inner join students on students.id=student_answers.fk_student where fk_question=?";
	
	public final static String SELECT_STUDENTS_WHO_SELECTED_ANSWER = "select username from answers as a inner join student_answers as sa on a.id=sa.fk_answer inner join students as s on s.id=sa.fk_student where a.id=?;";
	
	// TODO: add quiz db statements
	public final static String CREATE_QUIZ = "insert into quizzes (title, start_time, end_time, created, fk_class_code) values (?, ?, ?, Now(), (select id from class_codes where code=?));";
	
	public final static String CREATE_QUESTION = "insert into questions (question, fk_quiz, fk_question_type) values (?, (select id from quizzes where title=? and start_time=? and end_time=? and fk_class_code=(select id from class_codes where code=?)), ?);";
	
	public final static String CREATE_ANSWER = "insert into answers (answer, correct, fk_question) values (?, ?, (select id from questions where question=? and fk_quiz=(select id from quizzes where title=? and start_time=? and end_time=? and fk_class_code=(select id from class_codes where code=?)) and fk_question_type=?));";
	
	// TODO: edit quiz db statements
	public final static String EDIT_QUIZ_BY_ID = "update quizzes set title=?, start_time=?, end_time=? where id=?;";
	
	public final static String EDIT_QUESTION_BY_ID = "update questions set question=? where id=?;";
	
	public final static String EDIT_ANSWER_BY_ID = "update answers set answer=?, correct=? where id=?;";
	
	// TODO: delete quiz db statements
	public final static String DELETE_QUIZ_USING_LIST = "delete from quizzes where id in (";
	
	public final static String DELETE_QUESTION_USING_LIST = "delete from questions where id in (";
	
	public final static String DELETE_ANSWER_USING_LIST = "delete from answers where id in (";
	
	/* ------------------------- End of quiz DB statements ------------------------------------------ */
}
