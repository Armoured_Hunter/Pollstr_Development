/*
Author: 	Joshua Muysson
Author: 	Jinesh Bhatt
Class:		Web Enterprise Applications 
Project:	Pollster Assignment 4 Version-0.3
Teacher: 	Douglas King
Date:		28/03/2018
File:		ControllerModelLibrary.java
Purpose:	Return values for users. 	
*/

package library;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import org.json.simple.JSONObject;

public class ControllerModelLibrary {

	public static final String MODEL_ADDRESS = "127.0.0.1";
	public static final int MODEL_PORT = 8441;

	public static enum Permission {
		STUDENT, TEACHER, ADMIN
	};

	public static enum Types {
		MULTIPLE_CHOICE, TRUE_FALSE, SHORT_ANSWER
	}

	// uses username and permission to log in
	public static class LoginRequest implements Serializable {
		private static final long serialVersionUID = 1782916313203086L;
		private String username;
		private String studentCode;
		private Permission per;

		public LoginRequest(String username, Permission per) {
			this(username, "", per);
		}

		public LoginRequest(String username, String studentCode, Permission per) {
			this.username = username;
			this.studentCode = studentCode;
			this.per = per;
		}

		// returns the username
		public String getUsername() {
			return username;
		}

		// gets the code the student is using to log in
		public String getStudentCode() {
			return studentCode;
		}

		// returns the permission of the user
		public Permission getPer() {
			return per;
		}
	}

	// class for responding to the log in.
	public static class LoginResponse implements Serializable {
		
		private static final long serialVersionUID = 112893062340781608L;
		private int id;
		private String password;
		private String firstName;
		private String lastName;
		private boolean valid;
		private Map<String, String> classCodes;
		private Integer[] quizIDs;
		private String[] quizNames;

		// Invalid Login
		public LoginResponse(boolean valid) {
			this(-1, "", "", "", null, null, null, valid);
		}

		// Teacher Login
		public LoginResponse(String firstName, String lastName, String password, Map<String, String> classCodes, boolean valid) {
			this(-1, firstName, lastName, password, classCodes, null, null, valid);
		}

		// Student Login
		public LoginResponse(int ID, Integer[] quizIDs, String[] quizNames, boolean valid) {
			this(ID, "", "", "", null, quizIDs, quizNames, valid);
		}
		
		// Admin Login
		public LoginResponse(String firstName, String lastName, String password, boolean valid) {
			this(-1, firstName, lastName, password, null, null, null, valid);
		}

		private LoginResponse(int ID, String firstName, String lastName, String password, Map<String, String> classCodes, Integer[] IDs,
				String[] quizNames, boolean valid) {
			this.id = ID;
			this.password = password;
			this.valid = valid;
			this.firstName = firstName;
			this.lastName = lastName;
			this.classCodes = classCodes;
			this.quizIDs = IDs;
			this.quizNames = quizNames;
		}
		
		// return bool
		public boolean isValid() {
			return valid;
		}

		// returns the password
		public String getPassword() {
			return password;
		}

		public String getFirstName() {
			return firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public Map<String, String> getClassCodes() {
			return classCodes;
		}
		
		public Integer[] getQuizIDs() {
			return quizIDs;
		}

		public String[] getQuizNames() {
			return quizNames;
		}
		
		public int getID() {
			return id;
		}

	}

	// classes for signing up
	public static class SignUp implements Serializable {

		private static final long serialVersionUID = 4144223967055747479L;
		private String username;
		private String firstName;
		private String lastName;
		private String email;
		private String password;
		private Permission per;

		public SignUp(String username, String email, String password, String firstName, String lastName) {
			this(username, firstName, lastName, email, password, Permission.TEACHER);
		}

		public SignUp(String username, String email, String password, String firstName, String lastName,
				Permission per) {
			this.username = username;
			this.firstName = firstName;
			this.lastName = lastName;
			this.email = email;
			this.password = password;
			this.per = per;
		}

		// returns the username
		public String getUsername() {
			return username;
		}

		// returns the first name
		public String getFirstName() {
			return firstName;
		}

		// returns the last name
		public String getLastName() {
			return lastName;
		}

		// returns the email
		public String getEmail() {
			return email;
		}

		// returns the password
		public String getPassword() {
			return password;
		}

		// returns the permission
		public Permission getPer() {
			return per;
		}
	}

	// class for the responses to sign in response
	public static class SigninResponse implements Serializable {
		private static final long serialVersionUID = 1L;
		private boolean userNameValid;
		private boolean inserted;
		private String reason;
		// continue adding booleans as needed

		public SigninResponse(boolean userNameValid, boolean inserted, String reason) {
			this.userNameValid = userNameValid;
			this.inserted = inserted;
			this.reason = reason;
		}

		public boolean wasAdded() {
			return userNameValid && inserted;
		}

		public String getReason() {
			return reason;
		}
	}
	
	// account authorization request object
	public static class ValidationRequest implements Serializable {

		private static final long serialVersionUID = 5718949212034681419L;
		
		private String username;
		
		public ValidationRequest(String username) {
			this.username = username;
		}
		
		public String getUsername() {
			return username;
		}
		
	}
	
	public static class ValidationComplete implements Serializable {

		private static final long serialVersionUID = -5158688600593575919L;
		
		private boolean verified;
		
		public ValidationComplete(boolean verified) {
			this.verified = verified;
		}
		
		public boolean getVerified() {
			return verified;
		}
		
	}
	
	// uses username to ask for data
	public static class AdminDataRequest implements Serializable {

		private static final long serialVersionUID = -9006001511458980503L;
		
	}
	
	// class for getting data for admin 
	public static class DatabaseData implements Serializable {

		private static final long serialVersionUID = 1L;
		private ResultSet codeList;
		private ResultSet teacherInfo;
		
		
		public ResultSet getCodeList() {
			return codeList;
		}

		public ResultSet getTeacherInfo() {
			return teacherInfo;
		}

		
		public DatabaseData(ResultSet codeList, ResultSet teacherInfo) {
			this.codeList = codeList;
			this.teacherInfo = teacherInfo;
		}		
	}

	// class for creating a quiz
	public static class CreateQuiz implements Serializable {

		private static final long serialVersionUID = -7191962159243039659L;

		// class for storing answers
		public static class Answer implements Serializable {

			private static final long serialVersionUID = -6865451478725860642L;
			private boolean isCorrect;
			private String answer;

			public Answer(String answer, boolean isCorrect) {
				this.answer = answer;
				this.isCorrect = isCorrect;
			}

			public String getAnswer() {
				return answer;
			}

			public boolean getCorrectness() {
				return isCorrect;
			}
		}

		private String quizTitle;
		private Types quizType;
		private String classCode;
		private Timestamp startTime;
		private Timestamp endTime;
		private Map<String, Answer[]> questions = new HashMap<String, Answer[]>();

		public CreateQuiz(String quizTitle, Types quizType, String classCode, Timestamp startTime, Timestamp endTime,
				String[] questions, boolean[] answerValues, String[] answers) {

			int answerAmount = 1;

			this.quizTitle = quizTitle;
			this.quizType = quizType;
			this.classCode = classCode;
			this.startTime = startTime;
			this.endTime = endTime;

			switch (quizType) {
			case TRUE_FALSE:
				answerAmount = 2;
				break;
			case MULTIPLE_CHOICE:
				answerAmount = 5;
				break;
			case SHORT_ANSWER:
				answerAmount = 0;
			}

			for (int i = 0; i < questions.length; ++i) {
				this.questions.put(questions[i], createAnswers(i * answerAmount, answerAmount, answerValues, answers));
			}
		}

		private Answer[] createAnswers(int startPos, int amount, boolean[] answerValues, String[] answers) {

			Answer[] returnedList = new Answer[amount];

			for (int i = 0; i < amount; ++i) {
				if (answers[startPos + i].equals(""))
					break;
				returnedList[i] = new Answer(answers[startPos + i], answerValues[startPos + i]);
			}

			return returnedList;
		}

		/**
		 * @return the quizTitle
		 */
		public String getQuizTitle() {
			return quizTitle;
		}

		/**
		 * @return the quizType
		 */
		public Types getQuizType() {
			return quizType;
		}

		/**
		 * @return the classID
		 */
		public String getClassCode() {
			return classCode;
		}

		/**
		 * @return the startTime
		 */
		public Timestamp getStartTime() {
			return startTime;
		}

		/**
		 * @return the endTime
		 */
		public Timestamp getEndTime() {
			return endTime;
		}

		/**
		 * @return the questions
		 */
		public Map<String, Answer[]> getQuestions() {
			return questions;
		}

	}

	// class for creating quiz
	public static class QuizCreated implements Serializable {

		private static final long serialVersionUID = -7191962159244232887L;

		private boolean inserted;
		private String reason;

		public QuizCreated(boolean inserted, String reason) {
			this.inserted = inserted;
			this.reason = reason;
		}

		public boolean wasAdded() {
			return inserted;
		}

		public String getReason() {
			return reason;
		}

	}

	// class for creating new class code
	public static class NewClassCode implements Serializable {

		private static final long serialVersionUID = -3605943450529433279L;

		private String code;
		private String username;

		public NewClassCode(String username, String code) {
			this.code = code;
			this.username = username;
		}

		/**
		 * @return the code
		 */
		public String getCode() {
			return code;
		}

		/**
		 * @return the username
		 */
		public String getUsername() {
			return username;
		}

	}

	// class for response when a class code is added
	public static class ClassCodeAdded implements Serializable {

		private static final long serialVersionUID = 337293613556357148L;

		private boolean inserted;

		public ClassCodeAdded(boolean inserted) {
			this.inserted = inserted;
		}

		public boolean wasAdded() {
			return inserted;
		}
	}
	
	public static class AssignClassNames implements Serializable{

		private static final long serialVersionUID = -2254731868783532388L;
		
		String[] names;
		String[] codes;
		
		public AssignClassNames(String[] names, String[] codes) {
			this.names = names;
			this.codes = codes;
		}
		
		public String[] getNames() {
			return names;
		}
		
		public String[] getCodes() {
			return codes;
		}
	}

	// class for getting information on a specific quiz
	public static class FetchQuizWithID implements Serializable {

		private static final long serialVersionUID = -6389091724504932691L;

		private int iD;

		public FetchQuizWithID(int iD) {
			this.iD = iD;
		}

		public int getID() {
			return iD;
		}
	}

	// class for getting quiz contents
	public static class QuizContents implements Serializable {

		private static final long serialVersionUID = -6389091724505948641L;

		private Map<String, String[]> questions;
		private Types questionType;

		public QuizContents(Map<String, String[]> questions, Types questionType) {
			this.questions = questions;
			this.questionType = questionType;
		}

		public Map<String, String[]> getQuestions() {
			return questions;
		}

		public Types getQuestionType() {
			return questionType;
		}
	}
	
	// class sending student answers
	public static class StudentAnswers implements Serializable {

		private static final long serialVersionUID = -5746144546753732461L;
		
		private int quizID;
		private int studentID;
		private Map<String, String> questionAndAnswers;
		
		public StudentAnswers(int studentID, int quizId, Map<String, String> questionAndAnswers) {
			this.studentID = studentID;
			this.quizID = quizId;
			this.questionAndAnswers = questionAndAnswers;
		}
		
		public int getQuizID() {
			return quizID;
		}
		
		public Map<String,String> getQuestionsAndAnswers(){
			return questionAndAnswers;
		}
		
		public int getStudentID() {
			return studentID;
		}
	}
	
	// class for a response when the answers are added
	public static class AnswersAdded implements Serializable{

		private static final long serialVersionUID = -6973018574192942633L;
		
		private boolean added;
		
		public AnswersAdded(boolean added) {
			this.added = added;
		}
		
		public boolean getAdded() {
			return added;
		}
	}
	
	// class to request data in regards to teacher
	public static class RequestDataFromTeacher implements Serializable{
		private static final long serialVersionUID = -1L;
		private String userName;
		
		public RequestDataFromTeacher(String userName) {
			this.userName = userName;
		}
		
		public String getUserName() {
			return userName;
		}
	}
	
	// class send data as a json to controller
	public static class TeachersData implements Serializable{
		private static final long serialVersionUID = -1L;
		private JSONObject teacherInfo;
		
		public TeachersData(JSONObject teacherInfo) {
			this.teacherInfo = teacherInfo;
		}
		
		public JSONObject getTeacherData() {
			return teacherInfo;
		}
	}	
	
	public static class UpdateTeachersDatabase implements Serializable{
		
		private static final long serialVersionUID = -6923018574192946764L;
		
		private JSONObject toUpdate;
		
		public UpdateTeachersDatabase(JSONObject toUpdate) {
			this.toUpdate = toUpdate;
		}
		
		public JSONObject getToUpdate() {
			return toUpdate;
		}
	}
	
	public static class TeacherDatabaseUpdated implements Serializable {
		
		private static final long serialVersionUID = -6973018574192942153L;
		
		private boolean confirmed;
		private JSONObject data;
		
		public TeacherDatabaseUpdated(boolean confirmed) {
			this.confirmed = confirmed;
		}
		
		public TeacherDatabaseUpdated(JSONObject data, boolean confirmed) {
			this.confirmed = confirmed;
			this.data = data;
		}
		
		public JSONObject getData() {
			return data;
		}
		
		public boolean wasUpdated() {
			return confirmed;
		}
	}
}
