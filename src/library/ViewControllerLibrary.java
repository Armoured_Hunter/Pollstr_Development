/*
Author: 	Joshua Muysson
Class:		Web Enterprise Applications 
Project:	Pollster Assignment 4 Version-0.3
Teacher: 	Douglas King
Date:		28/03/2018
File:		ViewControllerLibrary.java
Purpose:	sets all the user info needed.	
*/

package library;

public class ViewControllerLibrary {
	
	// Variables to be used for e-mailing use
	public static final String EMAILHOST = "test@gmail.com";
	public static final String EMAILSUBJECT = "Welcome to Pollstr";
	public static final String EMAILMESSAGE = "This is a confirmation message<br><br>\n"
											+ "Please click on this link to complete your account creation: "
											+ "<a href=%s>%s</a><br><br>\n"
											+ "Thank you for joining the future of quizzing software";
	public static final String EMAILURL = "%s/%s?locale=%s&%s=%s";
	
	// session variables
	public final static int MAXINACTIVETIMEOUT = 15 * 60;
	public final static String USERLANG = "Locale";
	public final static String SESSIONID = "SessionID"; 
	public final static String SESSIONPERMISSION = "Session Permission";
	
	// teacher session variables
	public final static String SESSIONFIRSTNAME = "Session first name";
	public final static String SESSIONLASTNAME = "Session last name";
	public final static String SESSIONCODES = "get class codes";
	
	// student session variables
	public final static String SESSIONSTUDENTID = "Student ID";
	public final static String SESSIONQUIZIDS = "Quiz IDs";
	public final static String SESSIONQUIZZES = "Quiz Names";
	
	// Login variables
	public final static String LOGINUSERNAME = "Login";
	public final static String PASSWORD = "Password";
	public final static String DEFAULTPHRASE = "";
	public final static String INVALIDLOGINPHRASE = "Username and/or password is invalid. Please Try Again";
	public final static String UNABLETOCONNECTLOGINPHRASE = "Unable to login due to connection error. Please try again later";
	public final static String INVALIDLOGIN = "InvalidLogin";
	
	// Account creation variables
	public final static String NEWUSERNAME = "Username";
	public final static String FIRSTNAME = "First_Name";
	public final static String LASTNAME = "Last_Name";
	public final static String EMAIL = "Email_Address";
	public final static String NEWPASSWORD = "Passowrd";
	public final static String VERIFYPASSWORD = "Verify_password";
	public final static String UNABLETOCONNECTSIGNINPHRASE = "Unable to connect to the database to register the information.";
	public final static String INVALIDSIGNUP = "InvalidSignUp";
	
	// Account verification
	public final static String VERIFYCODE = "accountCode";
	
	// Teacher management variables
	public final static String NEWCODEREPORT = "ReportCode";
	public final static String REPORTEDJSONDATA = "Collected JSON";
	public final static String EDITJSONDATA = "EdittedData";
	public final static String REPORTDATABASEUPDATE = "Check Database";
	public final static String DATABASEUPDATEPASSED = "The changes have been successfully added";
	public final static String DATABASEUPDATEFAILED = "Unfortunately, there was an issue recording the updated to the database";
	
	// Different permissions for users. Helps streamline logins if permissions are provided
	public final static String PERMISSION = "permission";
	public final static String PERMISSIONSTUDENT = "student";
	public final static String PERMISSIONTEACHER = "teacher";
	public final static String PERMISSIONADMIN = "admin";
	
	// Quiz types
	public final static String TRUEFALSE = "True or False";
	public final static String MULTIPLE = "Multiple answer";
	public final static String SHORT = "Short answer";
	
	// Quiz creation variables
	public final static String QUIZANSWERS = "value";
	public final static String QUIZCORRECT = "vote";
	public final static String QUIZNAME = "QuizName";
	public final static String QUIZQUESTION = "QuestionName";
	public final static String QUIZSTARTTIME = "Start";
	public final static String QUIZENDTIME = "End";
	public final static String QUIZCLASSCODE = "classcode";
	public final static String QUIZTYPE = "type";
	public final static String QUIZERROR = "Quiz Not Created";
	public final static String QUIZERRORMESSAGE = "Cannot connect";
	
	// Student getting quiz variables
	public final static String STUDENTSELECTEDID = "Get the ID";
	public final static String STUDENTSELECTEDQUIZ = "QuizName";
	public final static String STUDENTQUESTIONS = "QuestionsToAdd";
	
	public final static String STUDENTANSWERED = "selected answer";
	public final static String ANSWEREDQUESTION = "question";
	
}
