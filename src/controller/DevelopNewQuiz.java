/*
Author: 	Joshua Muysson
Class:		Web Enterprise Applications 
Project:	Pollster Assignment 4 Version-0.3
Teacher: 	Douglas King
Date:		28/03/2018
File:		DevelopNewQuiz.java
Purpose:	Sends data needed for new quizzes to the database
 */

package controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import library.ViewControllerLibrary;
import library.ControllerModelLibrary.*;

public class DevelopNewQuiz extends HttpServlet{

	private static final long serialVersionUID = -6850981130325986560L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// temporary strings and objects to hold data
		String timeFormat = "yyyy-MM-dd'T'HH:mm";
		ModelConnection connect = null;
		String quizTitle = request.getParameter(ViewControllerLibrary.QUIZNAME);
		String[] questionNames = request.getParameterValues(ViewControllerLibrary.QUIZQUESTION);
		String[] answers = request.getParameterValues(ViewControllerLibrary.QUIZANSWERS);
		String correctAnswer = request.getParameter(ViewControllerLibrary.QUIZCORRECT);
		String classCode = request.getParameter(ViewControllerLibrary.QUIZCLASSCODE);
		Types quizType = Types.TRUE_FALSE;
		String togo = "TeacherPortal.jsp";

		Timestamp startTime;
		Timestamp endTime;
		boolean[] answerValues;

		if (answers == null) {
			answers = new String[0];
		}

		answerValues = new boolean[answers.length];

		// switch based quiz type to go to a specific page
		switch (request.getParameter(ViewControllerLibrary.QUIZTYPE)) {
		case ViewControllerLibrary.TRUEFALSE:
			quizType = Types.TRUE_FALSE;
			togo = "TeacherTF.jsp";
			break;
		case ViewControllerLibrary.MULTIPLE:
			quizType = Types.MULTIPLE_CHOICE;
			togo = "TeacherMP.jsp";
			break;
		case ViewControllerLibrary.SHORT:
			quizType = Types.SHORT_ANSWER;
			togo = "TeacherSA.jsp";
			break;
		}

		// iterrate through answers and add it to array
		for (int i = 0; i < answers.length; ++i) {
			if (quizType == Types.TRUE_FALSE)
				answerValues[i] = correctAnswer.equals(answers[i]);
			else
				answerValues[i] = request.getParameterValues(ViewControllerLibrary.QUIZCORRECT)[i].equals("TRUE");
		}

		try {

			// set the time stamps for start and end
			startTime = new Timestamp(new SimpleDateFormat(timeFormat).parse(request.getParameter(ViewControllerLibrary.QUIZSTARTTIME)).getTime());
			endTime = new Timestamp(new SimpleDateFormat(timeFormat).parse(request.getParameter(ViewControllerLibrary.QUIZENDTIME)).getTime());

			// create new model connection and send the new create quiz object
			connect = new ModelConnection();
			connect.sendObject(new CreateQuiz(quizTitle, quizType, classCode, startTime, endTime, questionNames, answerValues, answers));

			// look for incoming object from manager
			Object received = connect.recieveObject();

			if (received instanceof QuizCreated) {

				// Go back to home page
				response.sendRedirect("TeacherPortal.jsp");
				//request.getRequestDispatcher("TeacherPortal.jsp").forward(request, response);

			} else {

				// report the user was unable to create quiz
				request.setAttribute(ViewControllerLibrary.QUIZERROR, ViewControllerLibrary.QUIZERRORMESSAGE);

				// return to the current page
				request.getRequestDispatcher(togo).forward(request, response);
			}

		} catch (IOException | ClassNotFoundException | ServletException | ParseException e) {

			e.printStackTrace();

			// report the user was unable to login
			request.setAttribute(ViewControllerLibrary.QUIZERROR, ViewControllerLibrary.QUIZERRORMESSAGE);

			// return to the current page
			request.getRequestDispatcher(togo).forward(request, response);
		} finally {
			if (connect != null) {
				connect.closeConnection();
			}
		}
	}
}
