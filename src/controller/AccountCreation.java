/*
Author: 	Joshua Muysson
Class:		Web Enterprise Applications
Project:	Pollster Assignment 4 Version-0.3
Teacher: 	Douglas King
Date:		28/03/2018
File:		AccountCreation.java
Purpose:	Creates the accounts for teachers
 */

package controller;

import library.*;
import library.ControllerModelLibrary.*;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.mail.*;
import javax.mail.internet.*;
import com.sun.org.apache.xml.internal.security.utils.Base64;

public class AccountCreation extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		SigninResponse responded;

		// Sets fields for the new teacher.
		String username = request.getParameter(ViewControllerLibrary.NEWUSERNAME);
		String firstName = request.getParameter(ViewControllerLibrary.FIRSTNAME);
		String lastName = request.getParameter(ViewControllerLibrary.LASTNAME);
		String email = request.getParameter(ViewControllerLibrary.EMAIL);
		String password = request.getParameter(ViewControllerLibrary.NEWPASSWORD);
		String returnMessage = null;
		Permission per = Permission.TEACHER;
		ModelConnection connect = null;

		boolean sendEmail = true;

		try {

			// create new model connection and send the new sign up object
			connect = new ModelConnection();
			connect.sendObject(new SignUp(username, email, BCrypt.hashpw(password, BCrypt.gensalt()), firstName, lastName, per));

			// look for incoming object from manager
			Object received = connect.recieveObject();

			if (!(received instanceof SigninResponse)) {
				reportStatus(request, response, "Error with communications with the server");
				return;
			}

			if ((responded = (SigninResponse) received).wasAdded()) {		

				// for version 0.3. e-mailing new users.
				if (sendEmail) {
					try {
						String from = "auto.pollstr@gmail.com";

						String url = String.format(ViewControllerLibrary.EMAILURL, 
								request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf("/")), 
								"AuthenticateAccount", 
								request.getParameter(ViewControllerLibrary.USERLANG), 
								ViewControllerLibrary.VERIFYCODE, 
								encryptUsername(username));

						MimeMessage message = new MimeMessage(Session.getDefaultInstance(System.getProperties()));
						
						message.setFrom(new InternetAddress(from));
						message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
						message.setSubject(ViewControllerLibrary.EMAILSUBJECT);
						message.setContent(String.format(ViewControllerLibrary.EMAILMESSAGE, url, url), "text/html");

						GmailServices.sendMessage(GmailServices.getGmailService(), from, message);
						returnMessage = "Thank you for signing up.<br>A confirmation email has been sent";
						
					} catch (MessagingException | IOException e) {
						e.printStackTrace();
						returnMessage = "Account has been made but a confirmation e-mail could not be sent";
					}
				}

				// Go back to the home page or to a responder page
				reportStatus(request, response, returnMessage);

			} else {
				request.setAttribute(ViewControllerLibrary.NEWUSERNAME, username);
				request.setAttribute(ViewControllerLibrary.FIRSTNAME, firstName);
				request.setAttribute(ViewControllerLibrary.LASTNAME, lastName);
				request.setAttribute(ViewControllerLibrary.EMAIL, email);
				reportStatus(request, response, responded.getReason());
			}

		} catch (IOException | ClassNotFoundException | ServletException e) {
			request.setAttribute(ViewControllerLibrary.NEWUSERNAME, username);
			request.setAttribute(ViewControllerLibrary.FIRSTNAME, firstName);
			request.setAttribute(ViewControllerLibrary.LASTNAME, lastName);
			request.setAttribute(ViewControllerLibrary.EMAIL, email);
			reportStatus(request, response, ViewControllerLibrary.UNABLETOCONNECTSIGNINPHRASE);
		} finally {
			if (connect != null) {
				connect.closeConnection();
			}
		}
	}

	private void reportStatus(HttpServletRequest request, HttpServletResponse response, String message) throws ServletException, IOException {
		// report the user was unable to create an account for the provided reason
		request.setAttribute(ViewControllerLibrary.INVALIDSIGNUP, message);

		// return to the current page
		request.getRequestDispatcher("signinsignup.jsp").forward(request, response);
	}

	private String encryptUsername(String username) {
		
		return Base64.encode(username.getBytes());
	}

}
