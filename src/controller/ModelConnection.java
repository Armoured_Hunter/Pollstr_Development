/*
Author: 	Joshua Muysson
Class:		Web Enterprise Applications 
Project:	Pollster Assignment 4 Version-0.3
Teacher: 	Douglas King
Date:		28/03/2018
File:		ModelConnection.java
Purpose:	Sets up connection to the database.	
*/

package controller;

import library.ControllerModelLibrary;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class ModelConnection {

	private Socket connection;
	private ObjectInputStream input;
	private ObjectOutputStream output;
	
	// establish a connection to a provided default address to the database.
	public ModelConnection() throws UnknownHostException, IOException {
		this(ControllerModelLibrary.MODEL_ADDRESS, ControllerModelLibrary.MODEL_PORT);
	}
	
	// allows the connection with custom address and port.
	public ModelConnection(String address, int port) throws UnknownHostException, IOException {
		connection = new Socket(address, port);
		output = new ObjectOutputStream(connection.getOutputStream());
		input = new ObjectInputStream(connection.getInputStream());
	}

	// sends an object to the source of connection.
	public void sendObject(Object object) throws IOException {
		output.reset();
		output.writeObject(object);
	}

	// receives an object over ObjectInputStream
	public Object recieveObject() throws ClassNotFoundException, IOException {
		return input.readObject();
	}
	
	// close the the socket connection
	public void closeConnection() throws IOException {
		connection.close();
	}
}
