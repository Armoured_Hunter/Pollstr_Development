package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import library.ControllerModelLibrary.*;
import library.ViewControllerLibrary;

public class EditDatabaseServlet extends HttpServlet {

	private static final long serialVersionUID = 178624346284924L;
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		ModelConnection connect = null;
		String json = request.getParameter(ViewControllerLibrary.EDITJSONDATA);
		JSONParser parser = new JSONParser();
		
		TeacherDatabaseUpdated recievedData = null;
		
		System.out.println("I am being called.");

		try {
			connect = new ModelConnection();
			
			Object recieved;
			System.out.println(json);
			
			connect.sendObject(new UpdateTeachersDatabase((JSONObject)parser.parse(json)));
			
			// replace Object here with the corresponding library object
			if ((recieved = connect.recieveObject()) instanceof TeacherDatabaseUpdated && (recievedData = (TeacherDatabaseUpdated) recieved).wasUpdated()) {
				request.setAttribute(ViewControllerLibrary.REPORTDATABASEUPDATE, ViewControllerLibrary.DATABASEUPDATEPASSED);
				request.setAttribute(ViewControllerLibrary.REPORTEDJSONDATA, recievedData.getData().toJSONString());
				request.getRequestDispatcher("Dashboard.jsp?locale=" + request.getParameter(ViewControllerLibrary.USERLANG)).forward(request, response);
			} else {
				request.setAttribute(ViewControllerLibrary.REPORTDATABASEUPDATE, ViewControllerLibrary.DATABASEUPDATEFAILED);
				request.setAttribute(ViewControllerLibrary.REPORTEDJSONDATA, recievedData.getData().toJSONString());
				request.getRequestDispatcher("Dashboard.jsp?locale=" + request.getParameter(ViewControllerLibrary.USERLANG)).forward(request, response);
			}
			
		} catch (IOException | ClassNotFoundException | ParseException e) {
			request.setAttribute(ViewControllerLibrary.REPORTDATABASEUPDATE, ViewControllerLibrary.DATABASEUPDATEFAILED);
			request.setAttribute(ViewControllerLibrary.REPORTEDJSONDATA, recievedData.getData().toJSONString());
			request.getRequestDispatcher("Dashboard.jsp?locale=" + request.getParameter(ViewControllerLibrary.USERLANG)).forward(request, response);
			e.printStackTrace();
		} finally {
			if (connect != null)
				connect.closeConnection();
		}
		
	}
}
