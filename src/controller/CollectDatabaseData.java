/*
Author: 	Joshua Muysson
Class:		Web Enterprise Applications 
Project:	Pollster Assignment 4 Version-0.3
Teacher: 	Douglas King
Date:		28/03/2018
File:		CollectDataForTeacher.java
Purpose:	Gets data from the database. CURRENTLY NOT IN USE
*/

package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import library.ControllerModelLibrary.*;

public class CollectDatabaseData extends HttpServlet {

	private static final long serialVersionUID = 1464372428326652607L;

	public void doPost(HttpServletRequest request, HttpServletResponse responce) throws IOException, ServletException {

		ModelConnection connect = null;

		Object recieved;
		HttpSession session = request.getSession(false);
		
		if (session == null) {
			return;
		}
		
		try {
			connect = new ModelConnection();
			connect.sendObject(new AdminDataRequest());
			
			if ((recieved = connect.recieveObject()) instanceof DatabaseData) {
				request.setAttribute("", recieved);
				request.getRequestDispatcher("").forward(request, responce);
			}
			
		} catch (IOException | ClassNotFoundException | ServletException e) {
			e.printStackTrace();
			request.getRequestDispatcher("").forward(request, responce);
		} finally {
			if (connect != null) {
				connect.closeConnection();
			}
		}
	}
}
