/*
Author: 	Joshua Muysson
Class:		Web Enterprise Applications 
Project:	Pollster Assignment 4 Version-0.3
Teacher: 	Douglas King
Date:		28/03/2018
File:		CreateNewClassCode.java
Purpose:	Creates a class code for the teacher
*/

package controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import library.BCrypt;
import library.ControllerModelLibrary.*;
import library.ViewControllerLibrary;

public class CreateNewClassCode extends HttpServlet{

	private static final long serialVersionUID = -6850981130325986560L;

	@SuppressWarnings("unchecked")
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException  {
		// temporary objects and data types for  creation of new class code
		String username;
		String newCode = BCrypt.gensalt().substring(15, 20);
		Map<String, String> currentCodes;
		ModelConnection connect = null;
		ClassCodeAdded received;
		HttpSession session = request.getSession(false);
		boolean added = false;
		
		if (session == null) {
			// Have it go back to the front page
			response.sendRedirect("signinsignup.jsp");
			return;
		}
		
		username = (String) session.getAttribute(ViewControllerLibrary.SESSIONID);
		
		try {
			
			connect = new ModelConnection();
			
			while (!added) {
				connect.sendObject(new NewClassCode(username, newCode));
				
				received = (ClassCodeAdded) connect.recieveObject();
				if (!(added = received.wasAdded())) {
					newCode = BCrypt.gensalt().substring(15, 20);
				}
			}
			
			currentCodes = (HashMap<String, String>)session.getAttribute(ViewControllerLibrary.SESSIONCODES);
			currentCodes.put(newCode, null);
			
			session.setAttribute(ViewControllerLibrary.SESSIONCODES, currentCodes);
			
			request.setAttribute(ViewControllerLibrary.NEWCODEREPORT, newCode);
			// redirect to display the new code
			request.getRequestDispatcher("ClassCode.jsp?locale=" + request.getParameter(ViewControllerLibrary.USERLANG)).forward(request, response);
			
		} catch (IOException | ClassNotFoundException | ServletException e) {
			System.out.println(e.getMessage() + "\n" + e.toString());
			response.sendRedirect("TeacherPortal.jsp?connection=failed&locale=" + request.getParameter(ViewControllerLibrary.USERLANG) );
		} finally {
			if (connect != null) {
				connect.closeConnection();
			}
		}
	}
}