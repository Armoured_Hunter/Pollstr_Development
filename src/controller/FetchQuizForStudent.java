/*
Author: 	Joshua Muysson
Class:		Web Enterprise Applications
Project:	Pollster Assignment 4 Version-0.3
Teacher: 	Douglas King
Date:		28/03/2018
File:		FetchQuizForStudent.java
Purpose:	Fetches quiz information for a student.
*/

package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import library.ViewControllerLibrary;
import library.ControllerModelLibrary.*;

public class FetchQuizForStudent extends HttpServlet {

	private static final long serialVersionUID = 6314995474288468957L;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		// temporary objects and data types for quiz objects
		String iD = request.getParameter(ViewControllerLibrary.STUDENTSELECTEDID);
		ModelConnection connect = null;
		QuizContents quiz;
		
		try {
			// create a new model connection and send a fetch quiz with specific id
			connect = new ModelConnection();
			connect.sendObject(new FetchQuizWithID(Integer.parseInt(iD)));
			
			quiz = (QuizContents) connect.recieveObject();
			
			// set the attributes accordingly
			request.setAttribute(ViewControllerLibrary.STUDENTSELECTEDID, iD);
			request.setAttribute(ViewControllerLibrary.STUDENTSELECTEDQUIZ, request.getParameter(ViewControllerLibrary.STUDENTSELECTEDQUIZ));
			request.setAttribute(ViewControllerLibrary.STUDENTQUESTIONS, quiz.getQuestions());
			
			// based on type of question request and respond with different jsp
			switch (quiz.getQuestionType()) {
			case MULTIPLE_CHOICE:
				request.getRequestDispatcher("StudentMP.jsp").forward(request, response);
				break;
			case SHORT_ANSWER:
				request.getRequestDispatcher("StudentSA.jsp").forward(request, response);
				break;
			case TRUE_FALSE:
				request.getRequestDispatcher("StudentTF.jsp").forward(request, response);
				break;
			}
		} catch (IOException | ClassNotFoundException | ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (connect != null) {
				connect.closeConnection();
			}
		}
	}
}
