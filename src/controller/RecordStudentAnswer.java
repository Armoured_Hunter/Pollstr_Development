/*
Author: 	Joshua Muysson
Class:		Web Enterprise Applications 
Project:	Pollster Assignment 4 Version-0.3
Teacher: 	Douglas King
Date:		28/03/2018
File:		RecordStudentAnswer.java
Purpose:	Collects the answers from the student
*/

package controller;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import library.ControllerModelLibrary.*;
import library.ViewControllerLibrary;

public class RecordStudentAnswer extends HttpServlet {

	private static final long serialVersionUID = -4838173601567210083L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// temporary objects to record student information
		Object recieved;
		ModelConnection connect = null;
		int quizID = Integer.parseInt(request.getParameter(ViewControllerLibrary.STUDENTSELECTEDID));
		HashMap<String, String> studentAnswers = new HashMap<String, String>();
		String[] questions = request.getParameterValues(ViewControllerLibrary.ANSWEREDQUESTION);
		String[] answers = request.getParameterValues(ViewControllerLibrary.STUDENTANSWERED);
		
		// iterrate through questions & answers and add them to student answers array list
		for (int i = 0; i < questions.length; ++i) {
			studentAnswers.put(questions[i], answers[i]);
		}
		
		try {
			// get the session
			HttpSession session =  request.getSession(false);
			
			if (session == null) {
				response.sendRedirect("StudentLogin.jsp");
			}
			
			// create a new model connection and send a new Student Answers object
			connect = new ModelConnection();
			connect.sendObject(new StudentAnswers((int)session.getAttribute(ViewControllerLibrary.SESSIONSTUDENTID), quizID, studentAnswers));
			
			// look for incoming objects
			recieved = connect.recieveObject();
			
			// look for answers added object and react accordingly 
			if (recieved instanceof AnswersAdded && ((AnswersAdded) recieved).getAdded()) {
				
				response.sendRedirect("ThankYou.jsp");
			} else {
			
				ReportError(request, response);
			
			}

		} catch (IOException | ClassNotFoundException | ServletException e) {
			
			ReportError(request, response);
			
		} finally {
			if (connect != null) {
				connect.closeConnection();
			}
		}
	}
	
	// reports back with an error to student portal when error
	private void ReportError(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("StudentPortal.jsp").forward(request, response);
		
	}
}
