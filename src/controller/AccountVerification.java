/*
Author: 	Joshua Muysson
Class:		Web Enterprise Applications 
Project:	Pollster Assignment 4 Version-0.3
Teacher: 	Douglas King
Date:		28/03/2018
File:		AccountVerification.java
Purpose:	Verifies the account the user that accesses it 	
 */

package controller;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import library.ViewControllerLibrary;
import library.ControllerModelLibrary.*;

public class AccountVerification extends HttpServlet {

	private static final long serialVersionUID = 6814139344138305042L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

		String username;
		ModelConnection connect = null;
		Object recieved;

		try {
			
			username = decryptCode(request.getParameter(ViewControllerLibrary.VERIFYCODE));
			connect = new ModelConnection();
			connect.sendObject(new ValidationRequest(username));

			if ((recieved = connect.recieveObject()) instanceof ValidationComplete && ((ValidationComplete)recieved).getVerified()) {
				response.sendRedirect("Verified.jsp?locale=" + request.getParameter("locale"));
			} else {
				response.sendRedirect("Error.jsp?locale=" + request.getParameter("locale"));
			}
		} catch (IOException | ClassNotFoundException | Base64DecodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.sendRedirect("Error.jsp?locale=" + request.getParameter("locale"));
		}
		finally {
			connect.closeConnection();
		}
	}

	// Currently returns null. Will implement decryption algorithm at a later date.
	private String decryptCode(String code) throws Base64DecodingException {
		return new String(Base64.decode(code));
	}
}
