package controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import library.ControllerModelLibrary.*;
import library.ViewControllerLibrary;

public class UpdateClassCodeName extends HttpServlet {

	private static final long serialVersionUID = -8488074602754549656L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		ModelConnection connect = null;
		
		String[] names, codes;
		Map<String, String> sessionCodes = (Map<String, String>)request.getSession().getAttribute(ViewControllerLibrary.SESSIONCODES);
		
		try {

			connect = new ModelConnection();
			
			Object recieved;

			connect.sendObject(new AssignClassNames(names = request.getParameterValues(""), codes =request.getParameterValues("")));

			// replace Object here with the corresponding library object
			if ((recieved = connect.recieveObject()) instanceof ClassCodeAdded && ((ClassCodeAdded)recieved).wasAdded()) {
				for (int i = 0; i < codes.length; ++i) {
					sessionCodes.put(codes[i], names[i]);
				}
				request.getSession().setAttribute(ViewControllerLibrary.SESSIONCODES, sessionCodes);
				request.getRequestDispatcher("ClassCode.jsp").forward(request, response);
			} else {
				request.getRequestDispatcher("ClassCode.jsp").forward(request, response);
			}

		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			request.getRequestDispatcher("ClassCode.jsp").forward(request, response);
		} finally {
			if (connect != null)
				connect.closeConnection();
		}

	}
}
