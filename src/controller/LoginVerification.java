/*
Author: 	Joshua Muysson
Class:		Web Enterprise Applications 
Project:	Pollster Assignment 4 Version-0.3
Teacher: 	Douglas King
Date:		28/03/2018
File:		LoginVerification.java
Purpose:	Verifies the log in. 	
*/

package controller;

import library.*;
import library.ControllerModelLibrary.*;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginVerification extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// temporary  objects and data types to hold login verfication
		LoginResponse responded;
		String username = request.getParameter(ViewControllerLibrary.LOGINUSERNAME);
		String password = request.getParameter(ViewControllerLibrary.PASSWORD);
		Permission per = Permission.TEACHER;
		String reroute = null, togo = null;
		ModelConnection connect = null;
		
		// Switch case for deciding if the user is a student, teacher, or admin.
		switch (request.getParameter(ViewControllerLibrary.PERMISSION)) {
		case ViewControllerLibrary.PERMISSIONSTUDENT:
			per = Permission.STUDENT;
			reroute = "StudentSignIn.jsp?locale=" + request.getParameter(ViewControllerLibrary.USERLANG);
			togo = "StudentPortal.jsp?locale=" + request.getParameter(ViewControllerLibrary.USERLANG);
			break;
		case ViewControllerLibrary.PERMISSIONTEACHER:
			per = Permission.TEACHER;
			reroute = "signinsignup.jsp?locale=" + request.getParameter(ViewControllerLibrary.USERLANG);
			togo = "TeacherPortal.jsp?locale=" + request.getParameter(ViewControllerLibrary.USERLANG);
			break;
		case ViewControllerLibrary.PERMISSIONADMIN:
			per = Permission.ADMIN;
			reroute ="AdminSignIn.jsp?locale=" + request.getParameter(ViewControllerLibrary.USERLANG);
			togo = "AdminManage.jsp?locale=" + request.getParameter(ViewControllerLibrary.USERLANG);
			break;
		}

		try {
			
			// create new model connection and send login request object
			connect = new ModelConnection();
			connect.sendObject(new LoginRequest(username, (per == Permission.STUDENT? password:""), per));
			
			Object received = connect.recieveObject();
					
			if (received instanceof LoginResponse && 
					(responded = (LoginResponse) received).isValid() && (per == Permission.STUDENT || BCrypt.checkpw(password, responded.getPassword()))) {
				
				// if the password is correct.
				HttpSession session = request.getSession();
				if (session != null) {
					session.setAttribute(ViewControllerLibrary.SESSIONID, username);
					session.setAttribute(ViewControllerLibrary.USERLANG, request.getParameter(ViewControllerLibrary.USERLANG));
					switch (per) {
					case TEACHER:
						session.setAttribute(ViewControllerLibrary.SESSIONPERMISSION, ViewControllerLibrary.PERMISSIONTEACHER);
						session.setAttribute(ViewControllerLibrary.SESSIONFIRSTNAME, responded.getFirstName());
						session.setAttribute(ViewControllerLibrary.SESSIONLASTNAME, responded.getLastName());
						session.setAttribute(ViewControllerLibrary.SESSIONCODES, responded.getClassCodes());
						break;
					case STUDENT:
						session.setAttribute(ViewControllerLibrary.SESSIONPERMISSION, ViewControllerLibrary.PERMISSIONSTUDENT);
						session.setAttribute(ViewControllerLibrary.SESSIONSTUDENTID, responded.getID());
						session.setAttribute(ViewControllerLibrary.SESSIONQUIZIDS, responded.getQuizIDs());
						session.setAttribute(ViewControllerLibrary.SESSIONQUIZZES, responded.getQuizNames());
						break;
					case ADMIN:
						session.setAttribute(ViewControllerLibrary.SESSIONPERMISSION, ViewControllerLibrary.PERMISSIONADMIN);
						session.setAttribute(ViewControllerLibrary.SESSIONFIRSTNAME, responded.getFirstName());
						session.setAttribute(ViewControllerLibrary.SESSIONLASTNAME, responded.getLastName());						
						break;
					}
					// session.setMaxInactiveInterval(ViewControllerLibrary.MAXINACTIVETIMEOUT);
				} else {
					request.setAttribute(ViewControllerLibrary.INVALIDLOGIN, ViewControllerLibrary.INVALIDLOGINPHRASE);
					request.getRequestDispatcher(reroute).forward(request, response);
					return;
				}
				
				// Go to the users home page
				response.sendRedirect(togo);
				
			} else {
				// report the user was unable to login
				request.setAttribute(ViewControllerLibrary.INVALIDLOGIN, ViewControllerLibrary.INVALIDLOGINPHRASE);

				// return to the current page
				request.getRequestDispatcher(reroute).forward(request, response);
			}

		} catch (IOException | ClassNotFoundException | ServletException e) {
			
			// report the user was unable to login
			request.setAttribute(ViewControllerLibrary.INVALIDLOGIN, ViewControllerLibrary.UNABLETOCONNECTLOGINPHRASE);

			// return to the current page
			request.getRequestDispatcher(reroute).forward(request, response);
		} finally {
			if (connect != null) {
				connect.closeConnection();
			}
		}
	}

}
