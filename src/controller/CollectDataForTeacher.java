/*
Author: 	Joshua Muysson
Class:		Web Enterprise Applications 
Project:	Pollster Assignment 4 Version-0.3
Teacher: 	Douglas King
Date:		28/03/2018
File:		CollectDataForTeacher.java
Purpose:	Gets data from the database.
*/

package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import library.ViewControllerLibrary;
import library.ControllerModelLibrary.*;

public class CollectDataForTeacher extends HttpServlet {

	private static final long serialVersionUID = 1464372428326652607L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		ModelConnection connect = null;

		Object recieved;
		HttpSession session = request.getSession(false);
		
		if (session == null)
			response.sendRedirect("Welcome.jsp?locale=" + request.getParameter(ViewControllerLibrary.USERLANG));
		
		String teacherUsername = (String) session.getAttribute(ViewControllerLibrary.SESSIONID);
		
		try {
			connect = new ModelConnection();
			connect.sendObject(new RequestDataFromTeacher(teacherUsername));
			
			if ((recieved = connect.recieveObject()) instanceof TeachersData) {
				TeachersData data = (TeachersData) recieved;
				request.setAttribute(ViewControllerLibrary.REPORTEDJSONDATA, data.getTeacherData().toJSONString());
				request.getRequestDispatcher("Dashboard.jsp?locale=" + request.getParameter(ViewControllerLibrary.USERLANG)).forward(request, response);
			}
			else {
				request.getRequestDispatcher("TeacherPortal.jsp?locale=" + request.getParameter(ViewControllerLibrary.USERLANG)).forward(request, response);
			}
			
		} catch (IOException | ClassNotFoundException | ServletException e) {
			e.printStackTrace();
			request.getRequestDispatcher("TeacherPortal.jsp?locale=" + request.getParameter(ViewControllerLibrary.USERLANG)).forward(request, response);
		} finally {
			if (connect != null) {
				connect.closeConnection();
			}
		}
	}
}
