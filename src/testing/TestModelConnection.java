/*
Author:		Joshua Muysson
Author:		Jinesh Bhatt
Class:		Web Enterprise Applications 
Project:	Pollster Assignment 4 Version-ClassDemo
Teacher: 	Douglas King
Date:		2018-03-16
File:		TestModelConnection.java
Purpose:	Simply used of testing model behaviour.
 */
package testing;

import java.io.IOException;
import java.net.UnknownHostException;

import org.json.simple.JSONObject;

import controller.ModelConnection;
import library.ControllerModelLibrary.*;

public class TestModelConnection {

	public static void main(String[] args) throws UnknownHostException, IOException, ClassNotFoundException {

		ModelConnection connect = new ModelConnection();
		
		//Insert object here
		Object sender = new RequestDataFromTeacher("jmuysson");
		Object recieved;
		JSONObject data;
		
		connect.sendObject(sender);
		
		// replace Object here with the corresponding library object
		if ((recieved = connect.recieveObject()) instanceof TeachersData) {
			System.out.println("got something");
			data = ((TeachersData) recieved).getTeacherData();
			System.out.print(data.toJSONString());
		} else {
			System.out.println("Failed");
		}

		connect.closeConnection();
	}
}
