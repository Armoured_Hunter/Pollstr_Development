<!-- 
	Author: 	Arjun Sivaumar
	Class:		Web Enterprise Applications 
	Project:	Pollstr Assignment 7 Final Report / V1.0
	Teacher: 	Douglas King
	Date:		11/04/2018
	File:		Dashboard.jsp
	Purpose:	Teacher can perform analytics on their classes and quizzes
 -->
<%@page import="org.json.simple.JSONValue"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="library.ViewControllerLibrary, javax.servlet.http.HttpSession, org.json.simple.JSONObject, java.util.Map"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	 //Checks if the session timer ran out in the ViewControllerLibrary.java class
	if (session == null || session.getAttribute(ViewControllerLibrary.SESSIONID) == null) {
	 //Forwards to Welcome.jsp if authentication fails or session expires
	       request.getRequestDispatcher("/Welcome.jsp").forward(request, response);
	}
%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:set var="loc" value="en_US" />
<c:if test="${!(empty param.locale)}">
	<c:set var="loc" value="${param.locale}" />
</c:if>

<fmt:setLocale value="${loc}" />

<fmt:bundle basename="app">
	<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<!-- Bootstrap declaration -->
<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.4.css">
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/form-elements.css">
<link rel="stylesheet" href="assets/css/style.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Pollstr - Dashboard</title>
</head>
<body>
	<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
	<c:url value="TeacherPortal.jsp" var="TeacherPortal">
		<c:param name="locale" value="${loc}" />
	</c:url> <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="${TeacherPortal}">
		Pollstr Dashboard</a> <input class="form-control form-control-dark w-100"
		type="text" placeholder="Search" aria-label="Search">
	<div class="dropdown">
		<!-- Displays teacher name -->
		<button class="btn btn-secondary dropdown-toggle" type="button"
			data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<%=request.getSession().getAttribute(ViewControllerLibrary.SESSIONFIRSTNAME)%>
			<%=request.getSession().getAttribute(ViewControllerLibrary.SESSIONLASTNAME)%>
		</button>
		<ul class="dropdown-menu">
			<c:url value="Welcome.jsp" var="Welcome">
				<c:param name="locale" value="${loc}" />
			</c:url>
			<li class="dropdown-item" href="${Welcome}"><fmt:message
					key="logout" /></li>
		</ul>
	</div>
	</nav>

	<!-- Dropdown menus that will show all of the teachers classes and quizzes they created-->
	<div class="container-fluid">
		<div class="row">
			<nav class="col-md-2 d-none d-md-block bg-light sidebar">
			<div class="sidebar-sticky">
				<ul class="nav flex-column">
					<li><select id="codeSelect"
						name=<%=ViewControllerLibrary.QUIZCLASSCODE%>
						onchange="displayQuizData()">
							<option>Class Codes</option>
							<%
								Map<String, String> classes = (Map<String, String>) request.getSession()
											.getAttribute(ViewControllerLibrary.SESSIONCODES);

									for (Map.Entry<String, String> entry : classes.entrySet()) {

										out.println("<option value=" + entry.getKey() + ">"
												+ (entry.getValue() == null ? entry.getKey() : entry.getValue()) + "</option>");
									}
							%>
					</select></li>
				</ul>

			</div>
			</nav>

			<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
			<div
				class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
				<h1 class="h2">Dashboard</h1>
				<div class="btn-toolbar mb-2 mb-md-0">
					<!-- Teacher will be able to export the quiz data to specific formats -->
					<button class="btn btn-sm btn-outline-secondary dropdown-toggle">
						Export</button>
				</div>

			</div>


			<!-- Displays chart (static) --> <canvas class="my-4" id="myChart"
				width="900" height="380"></canvas>

			<h2>Quizzes</h2>
			<div class="table-responsive">
				<table id="quizzesTable" class="table table-sm">
					<thead>
						<tr>
							<th><center>Show Row</center></th>
							<th><center>Quiz Name</center></th>
							<th><center>Start Time</center></th>
							<th><center>End Time</center></th>
							<th><center>Edit</center></th>
							<th><center>Delete</center></th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>


		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
			integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
			crossorigin="anonymous"></script>
		<script>
			window.jQuery
					|| document
							.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')
		</script>
		<script src="../../../../assets/js/vendor/popper.min.js"></script>
		<script src="../../../../dist/js/bootstrap.min.js"></script>
		</script>
		<script
			src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
		<script
			src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script>
			function displayQuizData() {

				var selectedCode = document.getElementById("codeSelect").value;

				var students = [];
				var correct = [];
				var wrong = [];

				var json =
		<%=request.getAttribute(ViewControllerLibrary.REPORTEDJSONDATA)%>
			for (var i = 0; i < json.classes.length; i++) {

					if (json.classes[i].code == selectedCode) {

						for ( var j in json.classes[i].quizzes) {

							if (json.classes[i].quizzes.length >= 1) {

								var table = document
										.getElementById("quizzesTable");

								var row = table.insertRow(1);
								var cell1 = row.insertCell(0);
								var cell2 = row.insertCell(1);
								var cell3 = row.insertCell(2);
								var cell4 = row.insertCell(3);
								var cell5 = row.insertCell(4);
								var cell6 = row.insertCell(5)

								cell1.innerHTML = cell1.innerHTML = "<input id='"
										+ json.classes[i].quizzes[j].id
										+ "'class='showdata' type='checkbox' onClick='showQuizData(this.id)'/>";
								cell2.innerHTML = "<td contenteditable='false class='titleQuiz''>"
										+ json.classes[i].quizzes[j].title
										+ "</td>";
								cell3.innerHTML = "<td contenteditable='false'>"
										+ json.classes[i].quizzes[j].start_time
										+ "</td>";
								cell4.innerHTML = "<td contenteditable='false'>"
										+ json.classes[i].quizzes[j].end_time
										+ "</td>";
								cell5.innerHTML = "<button id='"
									+ json.classes[i].quizzes[j].id
									+ "'class='editbtn' type='button' class=' btn btn-primary'>Edit</button>";
								cell6.innerHTML = "<button id='"
									+ json.classes[i].quizzes[j].id
									+ "'class='deletebtn' type='button' class='btn btn-danger'>Delete</button>";

							}

							else {
								$("tbody").children().remove();
							}

						}

						for ( var p in json.classes[i].students) {

							students.push(json.classes[i].students[p].username);
							correct
									.push(json.classes[i].students[p].CorrectAnswers);
							wrong
									.push(json.classes[i].students[p].WrongAnswers);

						}

					}

				}


		      $(document).ready(function () {
		          $('.editbtn').click(function (event) {
		              var currentTD = $(this).parents('tr').find('td');
		              var id = event.target.id;
		              var json = <%=request.getAttribute(ViewControllerLibrary.REPORTEDJSONDATA)%>;
					  var selectedCode = document.getElementById("codeSelect").value;
					  var quizTitle = $('.titleQuiz').text();
					  var username = <%=request.getAttribute(ViewControllerLibrary.SESSIONID)%>;
					  var quizData = {"quiz":{"title":quizTitle, "starttime":"2018-03-20 20:52:02.0", "endtime":"2018-05-20 20:52:02.0", "id":id}};
					  var sendData = "<%= ViewControllerLibrary.EDITJSONDATA %>"+"="+JSON.stringify({"username":username,"Edit":{"quiz":[quizData]},"Delete":{}});
		              console.log(event.target.id)
		              
		              
		              if ($(this).html() == 'Edit') {
		                  currentTD = $(this).parents('tr').find('td');
		  				  var id = $(this).id;
		                  console.log(id);
		                  $.each(currentTD, function () {
		                      $(this).prop('contenteditable', true)
		                  });
		              } else {
		                 $.each(currentTD, function () {
		                      $(this).prop('contenteditable', false)
		                  });
		              }
		    
		              $(this).html($(this).html() == 'Edit' ? 'Save' : 'Edit')
		              
		        	  	$.ajax({
					  		type:"POST",
					        url: "EditDatabaseServlet",
					        data: sendData,
					        dataType: "text",
					        success: function(status){
					            console.log("Entered",status);
					            row.remove();
					        },
					        error:function(error){
					            console.log("error",error);
					        }
					    });
		    
		          });
		    
		      });
		      
		      $(document).ready(function() {
					$('.deletebtn').click(function(event) {
						var row = $(this).parent().parent();					
					  	var username = <%=request.getAttribute(ViewControllerLibrary.SESSIONID)%>;
					  	var quizCodesID = event.target.id;
					  	var sendData = "<%= ViewControllerLibrary.EDITJSONDATA %>"+"="+JSON.stringify({"username":username,"Edit":{},"Delete":{"quiz":[quizCodesID]}});
					  	
						
					  	console.log(quizCodesID);
					  	$.ajax({
					  		type:"POST",
					        url: "EditDatabaseServlet",
					        data: sendData,
					        dataType: "text",
					        success: function(status){
					            console.log("Entered",status);
					            row.remove();
					        },
					        error:function(error){
					            console.log("error",error);
					        }
					    });
						
					});
				});	

				var barChartData = {

					labels : students,
					datasets : [ {
						label : 'Correct Answers',
						backgroundColor : 'green',
						data : correct

					}, {
						label : 'Wrong Answers',
						backgroundColor : 'red',
						data : wrong
					} ]

				};

				var ctx = document.getElementById("myChart");
				var myChart = new Chart(ctx, {
					type : 'horizontalBar',
					data : barChartData,
					options : {
						title : {
							display : true,
							text : 'Student performance for class '
									+ selectedCode
						},
						tooltips : {
							mode : 'index',
							intersect : false
						},
						reponsive : true,
						scales : {
							xAxes : [ {
								stacked : true,
							} ],
							yAxes : [ {
								stacked : true
							} ]
						}
					}

				});
			}
			
			function showQuizData(id) {
				if(document.getElementById(id).checked) {
					var json =
						<%=request.getAttribute(ViewControllerLibrary.REPORTEDJSONDATA)%>
							console.log(json);
					var classCode = document.getElementById("codeSelect").value;		
					var table = document
					.getElementById("quizzesTable");
					var quizID = id;
					
					for(var i = 0; i < json.classes.length; i++) {
						
						if(json.classes[i].code == classCode) {
							
							for(var j in json.classes.quizzes) {
								
								if(json.classes[i].quizzes[j].id == quizID) {
									
								}
								
							}
							
						}
						
					}
					
				}
				else {
					return;
				}
			}
			
		</script>
</body>
</fmt:bundle>
</html>