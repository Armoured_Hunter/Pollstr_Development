<!-- 
Author: 	Gianpiero Beraldin
Class:		Web Enterprise Applications 
Project:	Pollstr Assignment 7 Final Report / V1.0
Teacher: 	Douglas King
Date:		11/04/2018
File:		ThankYou.jsp
Purpose:	thanks the student, returns to student portal
 -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="library.ViewControllerLibrary, javax.servlet.http.HttpSession"%>
<%
	// Checks if the session timer ran out in the ViewControllerLibrary.java class
	//if (session == null || session.getAttribute(ViewControllerLibrary.SESSIONID) == null) {
	// Forwards to Welcome.jsp if authentication fails or session expires
	//       request.getRequestDispatcher("/Welcome.jsp").forward(request, response);
	//}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:set var="loc" value="en_US" />
<c:if test="${!(empty param.locale)}">
	<c:set var="loc" value="${param.locale}" />
</c:if>

<fmt:setLocale value="${loc}" />

<fmt:bundle basename="app">

	<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Pollstr</title>
<!-- Bootstrap declaration -->
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/form-elements.css">
<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<%@include file="StudentNavBar.jsp"%>

	<!--  Thanks the student, allows them to return to the student portal. -->
	<div class="jumbotron text-xs-center">
	<center>
		<h1 class="display-3">
			<center>
				<fmt:message key="thankYou" />
			</center>
		</h1>
		<p class="lead">
			<strong><center>
					<fmt:message key="slogan" />
				</center></strong>
		</p>

		<c:url value="StudentPortal.jsp" var="StudentPortal">
			<c:param name="locale" value="${loc}" />
		</c:url>

		<a class="btn btn-primary btn-lg" href="${StudentPortal}"
			role="button"><center>
				<fmt:message key="goHome" />
			</center></a>
			</center>
	</div>
</body>
</fmt:bundle>
</html>
