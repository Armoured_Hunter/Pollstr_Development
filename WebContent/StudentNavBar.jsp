<!-- 
Author: 	Gianpiero Beraldin, Josh Muysson
Class:		Web Enterprise Applications 
Project:	Pollstr Assignment 7 Final Report / V1.0
Teacher: 	Douglas King
Date:		11/04/2018
File:		StudentNavBar.jsp
Purpose:	nav bar for students
 -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="library.ViewControllerLibrary, javax.servlet.http.HttpSession"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/form-elements.css">
<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>

	<!-- Sets up a navigation bar that displays on all windows. -->
	<nav class="navbar navbar-inverse navbar-static-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
				aria-expanded="false">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<!-- links the title on nav-bar to other web pages.-->

			<c:url value="StudentPortal.jsp" var="StudentPortal">
				<c:param name="locale" value="${loc}" />
			</c:url>

			<a class="navbar-brand" href="${StudentPortal}"><fmt:message
					key="titleBar" /></a>
		</div>

		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<!-- Languages drop down menu will show on the right side of nav-bar  -->
			<ul class="nav navbar-nav navbar-right">

				<c:url value="Welcome.jsp" var="Welcome">
					<c:param name="locale" value="${loc}" />
				</c:url>

				<a class="btn btn-primary btn" href="${Welcome}" role="button"><fmt:message
						key="logout" /></a>
				<p></p>
				<div class="dropdown">
					<span>Languages</span><span class="caret"></span>
					<div class="dropdown-content">
						<c:url value="StudentPortal.jsp" var="engURL">
							<c:param name="locale" value="en_US" />
						</c:url>
						<li><a href="${engURL}">English</a></li>

						<c:url value="StudentPortal.jsp" var="frURL">
							<c:param name="locale" value="fr_FR" />
						</c:url>
						<li><a href="${frURL}">Francais</a></li>
						<li><a href="#">Italiano</a></li>
						<li><a href="#">Deutsch</a></li>
					</div>
				</div>
			</ul>
		</div>
	</div>
	</nav>
</body>
</html>