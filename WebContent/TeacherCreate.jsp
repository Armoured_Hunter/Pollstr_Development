<!-- 
Author: 	Gianpiero Beraldin
Class:		Web Enterprise Applications 
Project:	Pollstr Assignment 7 Final Report / V1.0
Teacher: 	Douglas King
Date:		11/04/2018
File:		TeacherCreate.jsp
Purpose:	Teacher can create different types of quizzes
 -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="library.ViewControllerLibrary, javax.servlet.http.HttpSession"%>

<%
// Checks if the session timer ran out in the ViewControllerLibrary.java class
//if (session == null || session.getAttribute(ViewControllerLibrary.SESSIONID) == null) {
       // Forwards to Welcome.jsp if authentication fails or session expires
//       request.getRequestDispatcher("/Welcome.jsp").forward(request, response);
//}%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:set var="loc" value="en_US" />
<c:if test="${!(empty param.locale)}">
	<c:set var="loc" value="${param.locale}" />
</c:if>

<fmt:setLocale value="${loc}" />

<fmt:bundle basename="app">
	<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Pollstr</title>
<!-- Bootstrap declaration -->
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/form-elements.css">
<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>

	<%@include file="TeacherNavBar.jsp"%>

	<div class="jumbotron">
	<div class="container">
	<center>
	<div>
		<!-- all templates are complete, teachers can submit a working true false to the database  -->
		<ul class="nav nav-pills nav-stacked">
			<c:url value="TeacherTF.jsp" var="TeacherTF">
				<c:param name="locale" value="${loc}" />
			</c:url>
			<c:url value="TeacherMP.jsp" var="TeacherMP">
				<c:param name="locale" value="${loc}" />
			</c:url>
			<c:url value="TeacherSA.jsp" var="TeacherSA">
				<c:param name="locale" value="${loc}" />
			</c:url>
			<li class="nav-item"><a class="nav-link TF" href="${TeacherTF}"><fmt:message
						key="true" /> | <fmt:message key="false" /></a></li>
			<li class="nav-item"><a class="nav-link MC" href="${TeacherMP}"><fmt:message
						key="multipleChoice" /></a></li>
			<li class="nav-item"><a class="nav-link SA" href="${TeacherSA}"><fmt:message
						key="shortAnswer" /></a></li>
		</ul>
	</div>
	</center>
	</div>
	</div>
</body>
</fmt:bundle>
</html>