<!-- 
Author: 	Gianpiero Beraldin
Class:		Web Enterprise Applications 
Project:	Pollstr Assignment 7 Final Report / V1.0
Teacher: 	Douglas King
Date:		11/04/2018
File:		Welcome.jsp
Purpose:	Greets the user upon loading in
 -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<c:set var="loc" value="en_US" />
<c:if test="${!(empty param.locale)}">
	<c:set var="loc" value="${param.locale}" />
</c:if>

<fmt:setLocale value="${loc}" />

<fmt:bundle basename="app">
	<head>
	
	
	
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Pollstr</title>
<!-- Bootstrap declaration -->
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/form-elements.css">
<link rel="stylesheet" href="assets/css/style.css">
	</head>
	<body>
		<!-- Sets up a navigation bar that displays on all windows. -->
		<nav class="navbar navbar-inverse navbar-static-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<c:url value="Welcome.jsp" var="HomePage">
					<c:param name="locale" value="${loc}" />
				</c:url>
				<a class="navbar-brand" href="${HomePage}"><fmt:message
						key="titleBar" /></a>
			</div>
			<!-- Languages drop down menu will show on the right side of nav-bar  -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<p></p>
					<!--  in V0.2 french will translate the web page to french, other languages are just place holders. -->
					<div class="dropdown">
						<span>Languages</span><span class="caret"></span>
						<div class="dropdown-content">
							<c:url value="Welcome.jsp" var="engURL">
								<c:param name="locale" value="en_US" />
							</c:url>
							<li><a href="${engURL}">English</a></li>

							<c:url value="Welcome.jsp" var="frURL">
								<c:param name="locale" value="fr_FR" />
							</c:url>
							<li><a href="${frURL}">Francais</a></li>
							<li><a href="#">Italiano</a></li>
							<li><a href="#">Deutsch</a></li>
						</div>
					</div>
				</ul>
			</div>
		</div>
		</nav>
		
		
		<!-- Asks the user to choose role and continues to the sign in page.  -->
		
				<h1>
					<center>
						<fmt:message key="userSelector" />
					</center>
				</h1>
				<br> <br> <br>
				<p>
				<center>
					<!-- Goes to sign in page for the user -->

					<!-- Send locale to next page to keep language choice constant -->
					<c:url value="signinsignup.jsp" var="SignInSignUp">
						<c:param name="locale" value="${loc}" />
					</c:url>

					<c:url value="StudentSignIn.jsp" var="StudentSignIn">
						<c:param name="locale" value="${loc}" />
					</c:url>
					<a class="btn btn-primary btn-lg" href="${SignInSignUp}"
						role="button"><fmt:message key="teacher" /></a> <a
						class="btn btn-primary btn-lg" href="${StudentSignIn}"
						role="button"><fmt:message key="student" /></a>
				</center>
				</p>
	</body>
</fmt:bundle>
</html>