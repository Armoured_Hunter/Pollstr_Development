<!-- 
	Author: 	Gianpiero Beraldin, Arjun Sivaumar
	Class:		Web Enterprise Applications 
	Project:	Pollstr Assignment 7 Final Report / V1.0
	Teacher: 	Douglas King
	Date:		11/04/2018
	File:		AdminManage.jsp
	Purpose:	Admins can see teachers and info
 -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="library.ViewControllerLibrary, javax.servlet.http.HttpSession"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
// Checks if the session timer ran out in the ViewControllerLibrary.java class
//if (session == null || session.getAttribute(ViewControllerLibrary.SESSIONID) == null) {
       // Forwards to Welcome.jsp if authentication fails or session expires
//       request.getRequestDispatcher("/Welcome.jsp").forward(request, response);
//}%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:set var="loc" value="en_US" />
<c:if test="${!(empty param.locale)}">
	<c:set var="loc" value="${param.locale}" />
</c:if>

<fmt:setLocale value="${loc}" />
<fmt:bundle basename="app">

	<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Pollstr - Admin Manage</title>
<!-- Bootstrap declaration -->
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/form-elements.css">
<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>

	<%@include file="AdminNavBar.jsp"%>

	<!-- Extra pages will be made for each type of quiz -->
	<div class="col-md-3">
		<ul class="nav nav-pills nav-stacked">
			<li class="active"><a href="#"><fmt:message
						key="manageUsers" /></a></li>
			<li class="active"><a href="#"><fmt:message
						key="manageQuizzes" /></a>
		</ul>

	</div>
	<!-- Table to display teachers (static) -->
	<div style="margin-left: 23%; padding: 1px 16px; height: 1000px;">
		<div class="container">
			<h2>
				<fmt:message key="teachers" />
			</h2>
			<table class="table table-striped">
				<thead>
					<tr>
						<th><center>
								<fmt:message key="username" />
							</center></th>
						<th><center>
								<fmt:message key="firstName" />
							</center></th>
						<th><center>
								<fmt:message key="lastName" />
							</center></th>
						<th><center>
								<fmt:message key="email" />
							</center></th>
						<th><center>
								<fmt:message key="password" />
							</center></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<!-- All the links are static -->
						<td><a href="#">johnsmith</a></td>
						<td>John</td>
						<td>Smith</td>
						<td>john.smith@smith.com</td>
						<td>smith123</td>
					</tr>
					<tr>
						<td><a href="#">johnsmith2</a></td>
						<td>John</td>
						<td>Smith</td>
						<td>john.smith@smith.com</td>
						<td>smith123</td>
					</tr>
					<tr>
						<td><a href="#">johnsmith3</a></td>
						<td>John</td>
						<td>Smith</td>
						<td>john.smith@smith.com</td>
						<td>smith123</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

</body>
</fmt:bundle>
</html>