<!-- 
Author: 	Gianpiero Beraldin
Class:		Web Enterprise Applications 
Project:	Pollstr Assignment 7 Final Report / V1.0
Teacher: 	Douglas King
Date:		11/04/2018
File:		TeacherPortal.jsp
Purpose:	Teacher can create or manage their quizzes, as well as generating a new quiz code
 -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="library.ViewControllerLibrary, javax.servlet.http.HttpSession"%>

<%
// Checks if the session timer ran out in the ViewControllerLibrary.java class
//if (session == null || session.getAttribute(ViewControllerLibrary.SESSIONID) == null) {
       // Forwards to Welcome.jsp if authentication fails or session expires
//       request.getRequestDispatcher("/Welcome.jsp").forward(request, response);
//}%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:set var="loc" value="en_US" />
<c:if test="${!(empty param.locale)}">
	<c:set var="loc" value="${param.locale}" />
</c:if>
<fmt:setLocale value="${loc}" />
<fmt:bundle basename="app">
	<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Pollstr</title>
<!-- Bootstrap declaration -->
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/form-elements.css">
<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<%@include file="TeacherNavBar.jsp"%>
	<!-- Buttons allow the teacher to create or manage quizzes -->
	<div class="jumbotron">
		<div class="container">
		<center>
			<!-- Links to create, manage and generate a quiz code for teachers -->
			<c:url value="TeacherCreate.jsp" var="TeacherCreate">
				<c:param name="locale" value="${loc}" />
			</c:url>
			<c:url value="Dashboard.jsp" var="TeacherManage">
				<c:param name="locale" value="${loc}" />
			</c:url>
			<c:url value="ClassCode.jsp" var="ClassCode">
				<c:param name="locale" value="${loc}" />
			</c:url>
			<form action=CollectDataForTeacher method=post>
				<a class="btn btn-primary btn-lg" href="${TeacherCreate}"
					role="button"><fmt:message key="create" /></a>

			
				<button class="btn btn-primary btn-lg" type="submit" href="${TeacherManage}"
					role="button"><fmt:message key="manage" /> </button>
			</form>
			<br> <a class="btn btn-primary btn-lg" href="${ClassCode}"
				role="button"><fmt:message key="generateCode" /> </a> <br> <br>
				</center>
		</div>
	</div>
</body>
</fmt:bundle>
</html>