<!-- 
	Author: 	Gianpiero Beraldin, Arjun Sivaumar
	Class:		Web Enterprise Applications 
	Project:	Pollstr Assignment 7 Final Report / V1.0
	Teacher: 	Douglas King
	Date:		11/04/2018
	File:		TeacherManage.jsp
	Purpose:	Teacher manage the quizzes they created
 -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="library.ViewControllerLibrary, javax.servlet.http.HttpSession"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
// Checks if the session timer ran out in the ViewControllerLibrary.java class
//if (session == null || session.getAttribute(ViewControllerLibrary.SESSIONID) == null) {
       // Forwards to Welcome.jsp if authentication fails or session expires
//       request.getRequestDispatcher("/Welcome.jsp").forward(request, response);
//}%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:set var="loc" value="en_US" />
<c:if test="${!(empty param.locale)}">
	<c:set var="loc" value="${param.locale}" />
</c:if>

<fmt:setLocale value="${loc}" />

<fmt:bundle basename="app">

	<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Pollstr</title>
<!-- Bootstrap declaration -->
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/form-elements.css">
<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>

	<%@include file="TeacherNavBar.jsp"%>

	<!-- Extra pages will be made for each type of quiz -->
	<div class="col-md-3">
		<ul class="nav nav-pills nav-stacked">

			<c:url value="TeacherQuiz1.jsp" var="TeacherQuiz1">
				<c:param name="locale" value="${loc}" />
			</c:url>
			<li><a href="${TeacherQuiz1}">True False Quiz</a>
			<li><a href="#">Multiple Choice Quiz</a>
			<li><a href="#">Short answer</a>
		</ul>
	</div>
</body>
</fmt:bundle>
</html>