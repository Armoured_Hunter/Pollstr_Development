<!-- 
Author: 	Arjun Sivakumar
Class:		Web Enterprise Applications 
Project:	Pollstr Assignment 7 Final Report / V1.0
Teacher: 	Douglas King
Date:		11/04/2018
File:		TeacherQuiz1.jsp
Purpose:	Simple layout for teachers can view a sample quiz
 -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="library.ViewControllerLibrary"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Pollstr</title>
<!-- Bootstrap declaration -->
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/form-elements.css">
<link rel="stylesheet" href="assets/css/style.css">

</head>
<body>
	<%@include file="TeacherNavBar.jsp"%>
	<div class="col-md-3">
		<ul class="nav nav-pills nav-stacked">
			<li><a href="TeacherQuiz1.jsp">True False Quiz</a>
			<li><a href="#">Multiple Choice Quiz</a>
			<li><a href="#">Short answer</a>
		</ul>
	</div>

	<div style="padding: 1px 12px; height: 1000px;">
		<div class="container">
			<h2>True False Quiz</h2>
		</div>
	</div>
</body>
</html>