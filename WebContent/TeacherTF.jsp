<!-- 
Author: 	Gianpiero Beraldin, Joshua Muysson
Class:		Web Enterprise Applications 
Project:	Pollstr Assignment 7 Final Report / V1.0
Teacher: 	Douglas King
Date:		11/04/2018
File:		TeacherTF.jsp
Purpose:	Teacher can create a simple true false question
 -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="library.ViewControllerLibrary, controller.LoginVerification, java.util.Map"%>

<%
// Checks if the session timer ran out in the ViewControllerLibrary.java class
if (session == null || session.getAttribute(ViewControllerLibrary.SESSIONID) == null) {
       // Forwards to Welcome.jsp if authentication fails or session expires
       response.sendRedirect("Welcome.jsp");
}%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:set var="loc" value="en_US" />
<c:if test="${!(empty param.locale)}">
	<c:set var="loc" value="${param.locale}" />
</c:if>

<fmt:setLocale value="${loc}" />

<fmt:bundle basename="app">
	<html>
<head>
<style>
#snackbar {
    visibility: hidden;
    min-width: 250px;
    margin-left: -125px;
    background-color: #333;
    color: #fff;
    text-align: center;
    border-radius: 2px;
    padding: 16px;
    position: fixed;
    z-index: 1;
    left: 50%;
    bottom: 30px;
    font-size: 17px;
}

#snackbar.show {
    visibility: visible;
    -webkit-animation: fadein 0.1s, fadeout 0.1s 2.5s;
    animation: fadein 0.1s, fadeout 0.1s 2.5s;
}

@-webkit-keyframes fadein {
    from {bottom: 0; opacity: 0;} 
    to {bottom: 30px; opacity: 1;}
}

@keyframes fadein {
    from {bottom: 0; opacity: 0;}
    to {bottom: 30px; opacity: 1;}
}

@-webkit-keyframes fadeout {
    from {bottom: 30px; opacity: 1;} 
    to {bottom: 0; opacity: 0;}
}

@keyframes fadeout {
    from {bottom: 30px; opacity: 1;}
    to {bottom: 0; opacity: 0;}
}
</style>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Pollstr</title>
<!-- Bootstrap declaration -->
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/form-elements.css">
<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>

	<%@include file="TeacherNavBar.jsp"%>

	<!-- Simple display for the quiz title -->
	<div class="jumbotron">
		<div class="container">
			<center>
			<h2>
				<fmt:message key="true" />
				||
				<fmt:message key="false" />
			</h2>

			<!-- Shows a hint for the teacher to input a quiz name -->
			<form method=post action="NewQuiz" role=form>
				<input type="hidden" name="<%= ViewControllerLibrary.QUIZTYPE %>"
					value="<%= ViewControllerLibrary.TRUEFALSE %>"> <input
					name="<%= ViewControllerLibrary.QUIZNAME %>" type=text
					placeholder=<fmt:message key="quizName"/>> <br> <br>
				<!-- Allows the teacher to enter a date and time to start the quiz and a time to live value on the quiz -->
				<fmt:message key="from" />
				<input type="datetime-local"
					name=<%= ViewControllerLibrary.QUIZSTARTTIME%>>
				<fmt:message key="to" />
				<input type="datetime-local"
					name=<%= ViewControllerLibrary.QUIZENDTIME%>> <br> <br>

				
				<!-- Allows the input for the question title -->
				<input type=text name=<%= ViewControllerLibrary.QUIZQUESTION %>
					placeholder="Question"> <br> <br>

				<!-- Allows the teacher to select the correct answer to the question -->
				<table>
					<tr>
						<input name=<%= ViewControllerLibrary.QUIZCORRECT %> type=radio value=TRUE> <fmt:message key="true" />
						<input type="hidden" name=<%= ViewControllerLibrary.QUIZANSWERS %> value="TRUE" />
					</tr>
					<br>
					<tr>
						<input name=<%= ViewControllerLibrary.QUIZCORRECT %> type=radio value=FALSE> <fmt:message key="false" />
						<input type="hidden" name=<%= ViewControllerLibrary.QUIZANSWERS %> value="FALSE" />
					</tr>
				</table>
				
				 
				<br> <br>


				<!-- Shows the teacher the list of generated class codes available to assign to the quiz  -->
				<select name=<%=ViewControllerLibrary.QUIZCLASSCODE %>>
					<option>Class Codes</option>
					<%
					
					Map<String, String> classes = (Map<String, String>)request.getSession().getAttribute(ViewControllerLibrary.SESSIONCODES);
					
					for(Map.Entry<String, String> entry : classes.entrySet()){
						
						out.println("<option value=" + entry.getKey() + ">" + (entry.getValue() == null? entry.getKey():entry.getValue()) + "</option>");
					}
					%>
				</select> <br> <br>
				<!-- When the teacher clicks the button the quiz will be submitted and the teacher 
			is brought back the to the create manage page -->

				<br> <input class="btn btn-primary btn-lg" onclick="myFunction()"
					href="${TeacherPortal}" role=button type=submit
					value=<fmt:message key="submit"  />>
					
					<!-- Snackbar message for user -->
					<div id="snackbar">Quiz Created!</div>
					
					<!-- Displays the snackbar to the user for 3 seconds then goes away when the quiz is created  -->
					<script>
						function myFunction() {
    					var x = document.getElementById("snackbar");
    					x.className = "show";
    					setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
						}
					</script>
			</form>
			</center>
		</div>
	</div>
</body>
</fmt:bundle>
</html>