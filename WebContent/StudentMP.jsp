<!-- 
Author: 	Gianpiero Beraldin
Class:		Web Enterprise Applications 
Project:	Pollstr Assignment 7 Final Report / V1.0
Teacher: 	Douglas King
Date:		11/04/2018
File:		StudentMP.jsp
Purpose:	grabs the Multiple choice question for the student
 -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="library.ViewControllerLibrary, javax.servlet.http.HttpSession, java.util.Map"%>
<%
// Checks if the session timer ran out in the ViewControllerLibrary.java class
//if (session == null || session.getAttribute(ViewControllerLibrary.SESSIONID) == null) {
       // Forwards to Welcome.jsp if authentication fails or session expires
//       request.getRequestDispatcher("/Welcome.jsp").forward(request, response);
//}%>
<!DOCTYPE html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:set var="loc" value="en_US" />
<c:if test="${!(empty param.locale)}">
	<c:set var="loc" value="${param.locale}" />
</c:if>

<fmt:setLocale value="${loc}" />

<fmt:bundle basename="app">
	<html lang="en">
<head>
<meta charset="utf-8" />
<title>Quiz</title>
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/form-elements.css">
<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>


	<%@include file="StudentNavBar.jsp"%>

	<div class="jumbotron">
	<div class="container">
		<center>
			<h2>Multiple Choice</h2>
			<form action="RecordAnswers" method=post>
				<!-- uneditable, students can only see the quiz name -->
				<input type="text"
					value=<%= (
					request.getAttribute(ViewControllerLibrary.STUDENTSELECTEDQUIZ) == null? "\"First Quiz\"":
						"\""+request.getAttribute(ViewControllerLibrary.STUDENTSELECTEDQUIZ)+"\"") %>
					readonly><br>

				<!-- uneditable, students can only see the quiz question -->

				<!-- Grabs the question based on the class code the teacher assigned the quiz when it was created -->
				<input type="text"
					name=<%= ViewControllerLibrary.ANSWEREDQUESTION %>
					value=<%=(request.getAttribute(ViewControllerLibrary.STUDENTQUESTIONS) == null? "\"Is Canada a country?\"":
				"\""+((Map<String, String[]>)request.getAttribute(ViewControllerLibrary.STUDENTQUESTIONS)).entrySet().iterator().next().getKey()+"\"")%>
					readonly><br> <br>
				<!-- Radio buttons, students cannot choose both -->
				<table>
					<% 
				if (request.getAttribute(ViewControllerLibrary.STUDENTQUESTIONS) != null){
					for (String s:((Map<String, String[]>)request.getAttribute(ViewControllerLibrary.STUDENTQUESTIONS)).entrySet().iterator().next().getValue()){
						out.println("<tr>");
						out.println("<input name=\"" + ViewControllerLibrary.STUDENTANSWERED + "\" type=radio value=\"" + s + "\">" + s);
						out.println("<br>");
						out.println("<tr>");
					}
				}
					%>
				</table>
				<c:url value="ThankYou.jsp" var="ThankYou">
					<c:param name="locale" value="${loc}" />
				</c:url>
				<input class="btn btn-primary btn-lg" type=submit
					value=<fmt:message key="submit" />> <input type=hidden
					name="<%= ViewControllerLibrary.STUDENTSELECTEDID %>"
					value="<%= request.getAttribute(ViewControllerLibrary.STUDENTSELECTEDID) %>">
			</form>
			</center>
		</div>
	</div>
</body>
</fmt:bundle>
</html>