<!-- 
	Author: 	Arjun Sivakumar
	Class:		Web Enterprise Applications 
	Project:	Pollstr Assignment 7 Final Report / V1.0
	Teacher: 	Douglas King
	Date:		11/04/2018
	File:		AdminSignIn.jsp
	Purpose:	Adminss can sign in. 
 -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="library.ViewControllerLibrary, controller.LoginVerification"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:set var="loc" value="en_US" />
<c:if test="${!(empty param.locale)}">
	<c:set var="loc" value="${param.locale}" />
</c:if>

<fmt:setLocale value="${loc}" />

<fmt:bundle basename="app">

	<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Pollstr - Admin Login</title>

<!-- CSS -->
<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/form-elements.css">
<link rel="stylesheet" href="assets/css/style.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	</head>
	<body>

		<%@page import="java.security.cert.X509Certificate"%>
		<%
			Object o = request.getAttribute("javax.servlet.request.X509Certificate");
				if (o != null) {
					X509Certificate certs[] = (X509Certificate[]) o;
					X509Certificate cert = certs[0];
		%>
		<%=cert.getSubjectDN().getName()%>
		<!-- Sets up a navigation bar that displays on all windows. -->
		<nav class="navbar navbar-inverse navbar-static-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<!-- links the title on nav-bar to other web pages.-->

				<c:url value="AdminSignIn.jsp" var="HomePage">
					<c:param name="locale" value="${loc}" />
				</c:url>


				<a class="navbar-brand" href="${HomePage}"> <fmt:message
						key="titleBar" /></a>
			</div>
			<!-- Languages drop down menu will show on the right side of nav-bar  -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<p></p>
					<!--  in V0.2 french will translate the web page to french, other languages are just place holders. -->
					<div class="dropdown">
						<span>Languages</span><span class="caret"></span>
						<div class="dropdown-content">
							<c:url value="AdminSignIn.jsp" var="engURL">
								<c:param name="locale" value="en_US" />
							</c:url>
							<li><a href="${engURL}">English</a></li>

							<c:url value="AdminSignIn.jsp" var="frURL">
								<c:param name="locale" value="fr_FR" />
							</c:url>
							<li><a href="${frURL}">Francais</a></li>
							<li><a href="#">Italiano</a></li>
							<li><a href="#">Deutsch</a></li>
						</div>
					</div>
				</ul>
			</div>
		</div>
		</nav>
		<!-- Top content -->
		<div class="top-content">

			<div class="inner-bg">
				<div class="container">

					<div class="row">
						<div class="col-sm-8 col-sm-offset-2 text">

							<div class="description">
								<p></p>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-5">

							<div class="form-box">
								<div class="form-top">
									<div class="form-top-left">
										<h3>Login</h3>
									</div>
								</div>

								<!-- Form to login -->

								<div class="form-bottom">
									<form role="form" action="LoginVerification" method="post"
										class="login-form">
										<div class="form-group">
											<label class="sr-only" for="form-username">Username</label> <input
												type="text" name=<%=ViewControllerLibrary.LOGINUSERNAME%>
												placeholder=<fmt:message key="username" />
												class="form-username form-control" id="form-username"
												required>
										</div>
										<div class="form-group">
											<label class="sr-only" for="form-password">Password</label> <input
												type="text" name=<%=ViewControllerLibrary.PASSWORD%>
												placeholder=<fmt:message key="password" />
												class="form-password form-control" id="form-password"
												required>
										</div>


										<input type="hidden"
											name=<%=ViewControllerLibrary.PERMISSION%>
											value=<%=ViewControllerLibrary.PERMISSIONADMIN%> />

										<!-- Button to submit -->

										<c:url value="AdminManage.jsp" var="AdminManage">
											<c:param name="locale" value="${loc}" />
										</c:url>

										<button type="submit" class="btn" href="${AdminManage}">
											<fmt:message key="signIn" />
										</button>

										<%
											if (null != request.getAttribute("invalid")) {

													out.println(request.getAttribute("invalid"));

												}
										%>

									</form>
								</div>
							</div>


						</div>
					</div>
				</div>
			</div>
		</div>
		<script></script>

		<!-- Javascript -->
		<script src="assets/js/jquery-1.11.1.min.js"></script>
		<script src="assets/bootstrap/js/bootstrap.min.js"></script>
		<script src="assets/js/scripts.js"></script>

		<!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->


	</body>
		<%
			} else {
		%>
		Object was null.
		<%
			}
		%>
		

</fmt:bundle>