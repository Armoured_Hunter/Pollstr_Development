### Library
- ControllerModelLibrary.java

## Controller
- PollStrController.java

## View
- PollStrView.java

## Model
- Server.java
- ServerSocketRunnable.java
- DBHelper.java
- DBManager.java

### Importing TLS (Try this FIRST, if it doesn't work then follow the bottom instructions)

1. Replace the following files at Severs => "Tomcatv9.0 at localhost-config" folder with files in TLS Cert folder:

        - server.xml: 
            ensure the path for tls cert is properly set:
                kystoreFile="${user.home}/git/Pollstr_Development/TLS Cert/tls.keystore" 
        
        - web.xml: no changes 

### Generating TLS Cert
1. On command prompt run the following command:

        "C:\Program Files\Java\jre1.8.0_162\bin\keytool.exe" -genkey -alias tomcat -keyalg RSA -keystore "C:\Users\{your_name}\git\Pollstr_Development\TLS Cert\tls.keystore"

2. Fill in the following: 

        What is your first and last name? BLANK 
        What is the name of your organizational unit? Development Unit
        What is the name of your organization? Pollstr
        What is the name of your City or Locality? Ottawa
        What is the name of your State or Province? ON
        What is the two-letter country code for this unit?  CA
        Is CN=Unknown, OU=Development Unit, O=Pollstr, L=Ottawa, ST=ON, C=CA correct?  yes
        Enter key password for <tomcat> 
            (RETURN if same as keystore password): BLANK

### Editing the Server files        
1. Changes to server.xml:

        <Connector connectionTimeout="20000" port="8080" protocol="HTTP/1.1" redirectPort="443"/>
        <Connector  port="443" SSLEnabled="true" clientAuth="false" 
                    keystoreFile="${user.home}/git/Pollstr_Development/TLS Cert/tls.keystore" 
                    keystorePass="password" maxThreads="200" protocol="org.apache.coyote.http11.Http11NioProtocol" 
                    scheme="https" secure="true" sslProtocol="TLS"/>


2. Changes to web.xml:

        <security-constraint>
        	<web-resource-collection>
        		<web-resource-name>Protected Context</web-resource-name>
        		<url-pattern>/*</url-pattern>
        	</web-resource-collection>
        	<!-- auth-constraint goes here if you requre authentication -->
        	<user-data-constraint>
        		<transport-guarantee>CONFIDENTIAL</transport-guarantee>
        	</user-data-constraint>
        </security-constraint>


